# set :environment, "development"
set :output, { error: "log/cron_error_log.log", standard: "log/cron_log.log"}
# job_type :runner,  "cd :path && rvm use 2.1.3 do bin/rails runner -e :environment ':task' :output"

every :hour do
  rake 'cron:process_medications'
end

every 30.minutes do
	rake 'cron:send_pushes'
end

# every 1.minute do
# 	rake 'cron:test'
# end