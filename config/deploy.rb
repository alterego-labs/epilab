require 'bundler/capistrano'
require "rvm/capistrano"
require 'capistrano_colors'
require "whenever/capistrano"
set :whenever_command, "bundle exec whenever"


load 'deploy/assets'

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
set :deploy_via, :remote_cache

set :application,     "epilab"
set :deploy_server,   "95.85.8.115"
set :bundle_without,  [:development, :test]

set :using_rvm, true
set :rvm_type, :system

set :user,            "root"
set :use_sudo,        false
set :deploy_to,       "/var/www/#{application}"
set :unicorn_conf,  "#{deploy_to}/current/config/unicorn.rb"
set :unicorn_pid,   "#{deploy_to}/shared/pids/unicorn.pid"
set :stack, :unicorn
role :web,            deploy_server
role :app,            deploy_server
role :db,             deploy_server, :primary => true

set :rvm_ruby_string, "2.1.5"

set :scm,             :git

set :repository,      "git@bitbucket.org:alterego-labs/epilab.git"

before 'deploy:finalize_update', 'set_current_release'
task :set_current_release, :roles => :app do
  set :current_release, latest_release
end

after 'deploy:finalize_update', 'deploy:run_after_finalize_update'

namespace :deploy do
  desc "Start application"

  task :restart do
    run "if [ -f #{unicorn_pid} ] && [ -e /proc/$(cat #{unicorn_pid}) ]; then kill -USR2 `cat #{unicorn_pid}`; else cd #{deploy_to}/current && bundle exec unicorn -c #{unicorn_conf} -E #{rails_env} -D; fi"
  end

  task :start do
    run "bundle exec unicorn -c #{unicorn_conf} -E #{rails_env} -D"
  end

  task :stop do
    run "if [ -f #{unicorn_pid} ] && [ -e /proc/$(cat #{unicorn_pid}) ]; then kill -QUIT `cat #{unicorn_pid}`; fi"
  end

  desc "Copy production database configuration"
  task :run_after_finalize_update, :roles => [:app, :db, :web] do
    run "cp #{deploy_to}/shared/database.yml #{release_path}/config/database.yml"
  end

  namespace :assets do
    task :precompile, :roles => :web, :except => { :no_release => true } do
      from = source.next_revision(current_revision)
      if capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
        run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
      else
        logger.info "Skipping asset pre-compilation because there were no asset changes"
      end
    end
  end

  after "deploy:update_code", "delayed_job:restart"
end

namespace :db do
  desc "Reset Fake data"
  task :seed, roles: [:app, :db, :web] do
    run "cd #{deploy_to}/current; rvm use #{rvm_ruby_string} do bundle exec rake db:seed RAILS_ENV=production"
  end
end

namespace :delayed_job do
  desc "Restart the delayed_job process"
  task :restart, :roles => :app do
      run "cd #{deploy_to}/current; RAILS_ENV=#{rails_env} rvm use #{rvm_ruby_string} do bin/delayed_job restart"
  end

  task :start, :roles => :app do
      run "cd #{deploy_to}/current; RAILS_ENV=#{rails_env} rvm use #{rvm_ruby_string} do bin/delayed_job start"
  end

  task :stop, :roles => :app do
      run "cd #{deploy_to}/current; RAILS_ENV=#{rails_env} rvm use #{rvm_ruby_string} do bin/delayed_job stop"
  end
end