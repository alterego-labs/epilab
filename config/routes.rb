Epilab::Application.routes.draw do
  devise_for :users

  root to: 'application#welcome'
  match '/about' => 'application#about', via: :get
  match '/year_report_statistic' => 'application#year_report_statistic', via: :get
  # PATIENTS DASHBOARD
  get "/patient" => "patients#today", as: :patient_root
  resource :patient, only: [:show, :edit, :update] do
    resources :medicines
    resources :seizures
    resource :consultation

    get :statistic
    post "/medications/:id/taken" => "medications#taken", as: :take_medication
    post "/medications/:id/rejected" => "medications#rejected", as: :reject_medication
  end



  # ADMIN DASHBOARD
  namespace :admin do
    get "/" => "patients#index", as: :root

    resources :patients, only: [:index, :show] do
      get :search, on: :collection
    end

    resources :users

    resources :doctors, except: [:show] do
      post :sort, on: :collection
    end

    resource :clinic, only: [:show, :update]
    resources :dialogs do
      resources :consultations, only: :create, on: :member
    end

    resources :medicines, except: [:show] do
      post :sort, on: :collection
    end

    resources :dictionaries do
      collection do
        post :sort
      end
    end
  end

  # API METHODS
  namespace :api, defaults: { format: :json } do
    namespace :v1 do

      resources :patients, only: :show do
        member do
          get :calendar
          get :info
          get :graphics
          post :report
        end
        resources :medications, only: [:index] do
          collection do
            get :graphic
            get :today
            get :rejects
          end
          member do
            post :taken
            post :rejected
          end
        end
        resources :seizures, only: [:create, :show, :destroy, :update] do
          collection do
            get :graphic
            get :today
            get :daytime_statistic
            get :average_duration
          end
        end
        resources :patient_medicines, path: :medicines, only: [:index, :create, :destroy, :update]
        resources :consultations, only: [:index, :create]
      end
      resources :medicines, only: [:index, :create]
      resources :device_tokens, only: [:create]

      get "dictionary/seizure/types" => "dictionary#index", as: :seizure_types, defaults: { bookmark: "seizure_type" }
      get "dictionary/seizure/reasons" => "dictionary#index", as: :seizure_reasons, defaults: { bookmark: "seizure_reason" }
      get "doctors" => "doctors#index"
      get "clinic" => "clinic#show"

      devise_scope :user do
        post "user/login" => "devise/sessions#create", as: :login
        post "user/logout" => "devise/sessions#destroy", as: :destroy
        post "user/signup" => "devise/registrations#create", as: :signup
        post "user/update" => "devise/registrations#update", as: :update_profile
        post "user/password/restore" => "devise/passwords#create", as: :restore_password
      end

    end
  end
end
