source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.9'

# Use postgresql as the database for Active Record
gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.2'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# jQuery UI for the Rails 3.1+ asset pipeline
gem 'jquery-ui-rails'

# A mini view framework for console/irb that's easy to use, even while under its influence. Console goodies include a no-wrap table, auto-pager, tree and menu.
gem 'hirb'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

# Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup.
gem 'simple_form', branch: "bootstrap-3"

# Slim is a template language whose goal is reduce the syntax to the essential parts without becoming cryptic.
gem 'slim', '2.0.2'

# Extending MVC concept by some useful modules — Edit
gem 'alter_mvc', '0.1.0', github: 'alterego-labs/alter_mvc'

# Easy file attachment management for ActiveRecord
gem 'paperclip', '4.1.1'

# Authorization Gem for Ruby on Rails.
# gem 'cancan'
gem 'cancancan', '~> 1.9'

# Sprockets (what Rails 3.1 uses for its asset pipeline) supports LESS
gem "less-rails"

# Twitter Bootstrap for Rails 3.x - 4 Asset Pipeline
gem "twitter-bootstrap-rails", github: 'seyhunak/twitter-bootstrap-rails', branch: :bootstrap3

# Official Sass port of Bootstrap
gem 'bootstrap-sass', '~> 3.2.0'

# Flexible authentication solution for Rails with Warden.
gem 'devise', '3.4.1'

# Annotates Rails/ActiveRecord Models, routes, fixtures, and others based on the database schema.
gem 'annotate'

# Validate e-mail addreses against RFC 2822 and RFC 3696 with this Ruby on Rails plugin and gem.
gem 'validates_email_format_of'

gem 'acts_as_positioned', github: 'stager94/acts_as_positioned'
gem 'capistrano'
gem 'unicorn'

# A library for generating fake data such as names, addresses, and phone numbers.
gem 'faker'

gem 'russian'

gem 'whenever', require: false
gem 'angularjs-rails'

gem "active_model_serializers"

gem 'exception_notification', github: 'smartinez87/exception_notification'
gem 'slack-notifier'
# gem 'streamio-ffmpeg'
# gem 'local_time'

# Database based asynchronous priority queue system -- Extracted from Shopify
gem 'delayed_job', '4.0.6'
gem 'delayed_job_active_record', '4.0.3'
gem 'daemons'

gem 'ng-rails-csrf', '0.1.0'

gem 'wicked_pdf', github: 'mileszs/wicked_pdf'
gem "pdfkit"
gem 'svg-graph'

gem 'apns', github: 'stager94/APNS'

gem 'date_validator'
gem 'tzinfo-data'

# gem 'streamio-ffmpeg'
gem "paperclip-ffmpeg"
gem 'paperclip-av-transcoder'

gem 'dalli'
gem 'bcrypt-ruby', '~> 3.0.0'
gem 'wkhtmltopdf-binary'
group :test do
	gem 'minitest'
	# Collection of testing matchers extracted from Shoulda
	gem 'shoulda-matchers'

	# Acceptance test framework for web applications
	gem 'capybara', '2.2.1'

	# RSpec for Rails-3+
	gem 'rspec-rails'
	gem 'rspec-mocks'

	# A library for setting up Ruby objects as test data.
	gem 'factory_girl_rails'

	# Strategies for cleaning databases in Ruby. Can be used to ensure a clean state for testing.
	gem 'database_cleaner'

  # Test your ActionMailer and Mailer messages with Capybara
  # gem 'capybara-email'
end

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development do
	gem  'rvm-capistrano',  require: false
	gem 'capistrano_colors', '0.5.4', require: false
	gem "letter_opener"
end

group :development, :test do
	# Binding navigation commands for Pry to make a simple debugger
	gem "pry-nav"
	gem "pry-rails"

	# Mutes assets pipeline log messages.
	gem 'quiet_assets'

	# A very fast & simple Ruby web server
	gem 'thin'
end