namespace :cron do
	task process_medications: :environment do
		Scheduling::ProcessPatients.new.execute
	end

	task send_pushes: :environment do
		Notifications::Medications.new.execute
	end

	task test: :environment do
		Scheduling::ProcessPatients.new.delay.execute
		p "Test"
	end
end