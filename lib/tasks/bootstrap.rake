namespace :bootstrap do
	task create_admin: :environment do
		User.create first_name: Faker::Name.first_name, 
								last_name: Faker::Name.last_name, 
								email: "admin@example.com",
								password: :password,
								birthday: Date.parse("23.08.1994"),
								sex: "male",
								role: "admin"
	end

	task create_clinic_info: :environment do
		Clinic.first_or_create
	end

	task fake_patients: :environment do
    20.times do |i|
      u = User.create first_name: Faker::Name.first_name,
								      last_name: Faker::Name.last_name,
      								email: Faker::Internet.email,
      								password: :password,
      								birthday: (20..60).to_a.sample.years.ago,
      								sex: "male",
      								role: "patient"
    end
	end
end