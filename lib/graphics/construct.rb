module Graphics
	class Construct < AlterMvc::BasicUseCase
		attr_reader :items, :label, :patient_id, :process_func, :calculated_period

		def formatted_date(date)
			Russian::strftime date, "%d %b %Y"
		end

		def formatted_month(date)
			Russian::strftime date, "%b %Y"
		end

		def date_timestamp(date)
			date.beginning_of_day.to_i
		end

		def month_timestamp(date)
			date.beginning_of_month.beginning_of_day.to_i
		end

		def period_hash
			{
				start_at: patient.created_at.to_date,
				end_at: Time.zone.today
			}
		end

		def item_label
			label || :value
		end

		def patient
			@_patient ||= UserQuery.find_patient_by_id patient_id
		end

		def process_bunch(label, items, formatted_label = nil)
			process_func ? process_func.call(label, items) : { data: label, item_label => (items.count rescue 0), formatted_date: formatted_label }
		end

		def fetch_period
			
		end

		# CLASSES

		class Daily < Construct
			def period
				(period_hash[:start_at]..period_hash[:end_at]).to_a
			end

			def grouped
				@_grouped ||= items.group_by &:date				
			end

			def result
				calculated_period.inject([]) do |res, date|
					res << process_bunch(date_timestamp(date), grouped[date], formatted_date(date))
				end
			end
		end

		class Weekly < Construct
			def period
				(period_hash[:start_at]..period_hash[:end_at]).to_a.group_by {|d| d.strftime "%W %Y" }
			end

			def grouped
				@_grouped ||= items.group_by { |o| o.date.strftime "%W %Y" }
			end

			def result
				calculated_period.inject([]) do |res, (week, days)|
					res << process_bunch(date_timestamp(days.first), grouped[week], formatted_date(days.first))
				end
			end
		end

		class Monthly < Construct
			def period
				(period_hash[:start_at]..period_hash[:end_at]).to_a.group_by {|d| d.strftime "%m %Y" }
			end

			def grouped
				@_grouped ||= items.group_by { |o| o.date.strftime "%m %Y" }
			end

			def result
				calculated_period.inject([]) do |res, (month, days)|
					res << process_bunch(month_timestamp(days.first), grouped[month], formatted_month(days.first))
				end
			end
		end

	end
end