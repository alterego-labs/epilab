module Graphics
	class Intervals < AlterMvc::BasicUseCase
		
		attr_reader :user, :interval_piece, :beginning_at, :count

		def interval
			{ 
				beginning_at: (fetch_intervals.last[1].first rescue fetch_intervals.last),
				ended_at: (fetch_intervals.first[1].last rescue fetch_intervals.first)
			}
		end

		def intervals
			fetch_intervals.reverse
		end

	private

		def fetch_beginning_at
			Date.parse(beginning_at) rescue Date.tomorrow
		end

		def started_at
			Date.parse fetch_beginning_at
		end

		def intervals_array
			user.send("#{interval_piece}_statistic_interval")
		end

		def parse_count
			count ? count.to_i : 1
		end

		def fetch_intervals
			@_fetch_intervals ||= intervals_array.to_a
														.select {|k, v| v[0] < fetch_beginning_at rescue k < fetch_beginning_at }
														.reverse.first(parse_count)
		end

	end
end