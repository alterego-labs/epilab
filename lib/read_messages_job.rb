class ReadMessagesJob < Struct.new(:dialog_id, :reader_id)
	def perform
		Consultancy::MarkingRead.new(dialog_id: dialog_id, reader_id: reader_id).execute
	end
end