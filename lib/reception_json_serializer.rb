class ReceptionJsonSerializer
	class <<self
		def parse(value)
			array(value).select { |r| is_valid_object(r) }
		end

	private
		def array(value)
			value.is_a?(String) ? JSON.parse(value) : value
		end

		def is_valid_object(r)
			r["dosage"].present? && r["time"].present?
		end
	end
end