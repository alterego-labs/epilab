class FormHelper
	include AlterMvc::Concerns::Presentable

	attr_reader :view_context, :f

	def initialize(params = {})
    params.each { |k, v| instance_variable_set("@#{k}", v) }
  end

	def render_btn_group(name, collection = [], labels = [])
		render_view :btn_group, locals: { f: f, name: name, collection: collection, labels: labels }
	end

private
	def render_view(name, options = {})
		view_context.render(build_render_params(name, options)).html_safe
	end

	def build_render_params(name, options)
		{partial: build_view_path(name)}.merge options
	end

	def build_view_path(name)
		"#{view_namespace}/#{name}"
	end

	def view_namespace
		"shared/form"
	end

end