module Patients
	module Constants
		MEDICATION_STATES = [
			"scheduled",
			"taken",
			"rejected"
		]
	end
end