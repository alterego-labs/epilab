module GlobalConstants
	CLINIC_TABS = [
		"about",
		"contacts"
	]

	DEFAULT_CLINIC_TAB = "about"

	PERIOD_DISTANCE = {
		week: 1.week,
		month: 1.month,
		quarter: 3.months,
		half: 6.months,
		year: 1.year
	}.with_indifferent_access

	PERIODICITY_DISTANCE = {
		everyday: 1.day,
		day: 2.days,
		weekly: 1.week,
		fortnightly: 2.weeks,
		monthly: 1.month
	}.with_indifferent_access

	DICTIONARY_ITEM_TYPES = [
		"seizure_type",
		"seizure_reason"
	]
end