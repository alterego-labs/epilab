module TranslationHelper
	class <<self
		def confirm_destroy(object, options = {})
			i18n object, { scope: "helpers.confirms.destroy" }.merge(options)
		end

		def clinic_tab_name(tab)
			i18n tab, { scope: "helpers.clinic.tab.name" }
		end

		def sex(sex)
			i18n sex, { scope: "dictionary.sex" }
		end

		def role(role)
			i18n role, { scope: "dictionary.role" }
		end

		def default_error_message(type)
			i18n type, { scope: "activerecord.errors.defaults" }
		end

		def success_action(object, action_type, options = {})
			i18n "flash.#{object}.#{action_type}.success", options
		end

		def seizure_types
			i18n "dictionary.seizure_types"
		end

		def period(period)
			i18n period, scope: "dictionary.periods"
		end

		def dictionary_item(bookmark)
			i18n bookmark, scope: "activerecord.models.dictionary_items"
		end

		# API
		def not_authenticated
			i18n "devise.failure.not_found_in_database"
		end

		def unauthenticated
			i18n "devise.failure.unauthenticated"
		end

		def month_name(month_id)
			i18n("date.calendar_month_names")[month_id.to_i]
		end

		def periodicity(key)
			i18n key, { scope: "periodicity" }
		end

	private
		def i18n(key, options = {})
			I18n.t key, options
		end
	end
end