module Users
	module Constants
		ROLES = [
			"admin",
			"patient"
		]

		SEXES = [
			"male",
			"female"
		]

		DETAILS = [
			"is_watcher",
			"sex",
			"birthday",
			"latest_scheduled_date",
			"current_sign_in_at",
			"last_sign_in_at",
			"current_sign_in_ip",
			"last_sign_in_ip"
		]

		ROLES_CLASSES = {
			"patient" => :primary,
			"admin"   => :danger
		}
	end
end