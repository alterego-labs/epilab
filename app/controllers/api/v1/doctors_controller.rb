class API::V1::DoctorsController < ApiController
	authorize_resource
	
	def index
		doctors = Doctor.all
		render status: 200, json: doctors
	end
end