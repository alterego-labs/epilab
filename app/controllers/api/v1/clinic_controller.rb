class API::V1::ClinicController < ApiController
	authorize_resource

	def show
		@info = Clinic.first
		html = render_to_string "application/about", layout: "mobile", formats: "html"
		render status: 200, json: { page: html }, root: false
	end
end