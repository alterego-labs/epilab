class API::V1::ConsultationsController < ApiController
	authorize_resource
	load_and_authorize_resource :patient, class: User

	def create
		@question = Consultancy::AddingMessage.new(params: consultation_params, patient_id: @patient.id).execute
		has_errors? ? respond_with(@question) : render(status: 200, json: @question)
	end

	def index
		@consultations = dialog.consultations.includes(:author)
		Delayed::Job.enqueue ReadMessagesJob.new(dialog.id, @patient.id) if dialog.unread_by_patient > 0
		render status: 200, json: @consultations
	end

private
	def dialog
		@_dialog ||= DialogQuery.fetch_by_patient_id(@patient.id)
	end

	def consultation_params
		@_consultation_params ||= ConsultationParameterSanitizer.new params if params[:consultation]
	end

	def has_errors?
		@question.respond_to?(:errors) && !@question.errors.empty?
	end
end