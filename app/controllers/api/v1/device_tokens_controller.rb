class API::V1::DeviceTokensController < ApiController
	include ApplicationHelper

	def create
		DeviceTokens::Store.new(device_token: params[:device_token], user_id: current_user.id).execute
		render status: 200, json: {}
	end
end