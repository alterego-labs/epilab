class API::V1::SeizuresController < ApiController
	
	# load_and_authorize_resource :patient, class: "User"
	load_resource :patient, class: "User"

	before_filter :fetch_seizure, only: [:destroy, :update]

	def show
		@seizure = SeizureQuery.find_for(params[:patient_id], params[:id])
		render status: 200, json: @seizure
	end

	def graphic
		if params[:dx_chart]
			data = Api::Seizures::Statistic.new(patient_id: params[:patient_id], period_type: params[:period]).execute
		else
			data = Rails.cache.fetch(["seizures_graphic", params[:patient_id], params[:interval], params[:count], params[:from], @patient.updated_at], expires_in: 1.day) do 
				Api::Seizures::GroupedStatistic.new(patient_id: params[:patient_id], interval: params[:interval], count: params[:count], from: params[:from]).execute
			end
		end
		render json: data
	end

	def today
		seizures = SeizureQuery.today_for @patient.id
		render status: 200, json: seizures
	end

	def create
		@seizure = Patients::RecordSeizure.new(patient_id: params[:patient_id], params: seizure_params).execute
		has_errors? ? respond_with(@seizure) : render(status: 200, json: @seizure)
	end

	def daytime_statistic
		use_case = Rails.cache.fetch(["seizures_daytime_statistic", params[:patient_id], @patient.updated_at], expires_in: 1.day) do
			Api::Seizures::DayTimeStatistic.new(patient_id: params[:patient_id]).execute
		end
		render status: 200, json: use_case, root: false
	end

	def average_duration
		use_case = Rails.cache.fetch(["seizures_average_duration", params[:patient_id], params[:interval], params[:count], params[:from], @patient.updated_at], expires_in: 1.day) do 
			Api::Seizures::AverageDuration.new(patient_id: params[:patient_id], interval: params[:interval], count: params[:count], from: params[:from]).execute
		end
		render status: 200, json: use_case
	end

	def destroy
		@seizure = Seizure.find params[:id]
		@seizure.destroy
		render status: 200, json: { success: true }
	end

	def update
		@seizure = Seizure.find params[:id]
		@seizure.update params.require(:seizure).permit! if params[:seizure].present?

		
		if @seizure.valid?

			if params[:seizure][:video].present? && params[:seizure][:video].blank?
				@seizure.clear_video
			end
			render status: 200, json: @seizure
		else
			render status: 422, json: { success: false, errors: @seizure.errors.full_messages }
		end
	end

private
	def seizure_params
		@_seizure_params ||= SeizureParameterSanitizer.new params if params[:seizure]
	end

	def has_errors?
		@seizure.respond_to?(:errors) && !@seizure.errors.empty?
	end

	def fetch_seizure
		@seizure = Seizure.find params[:id]
	end

end