class API::V1::MedicinesController < ApiController
	authorize_resource

	def index
		medicines = current_user.available_medicines
		render status: 200, json: medicines
	end

	def create
		use_case = Medicines::Adding.new(params: new_medicine_params, user_id: current_user.id)
		if use_case.valid?
			render status: 200, json: use_case.execute
		else
			render status: 401, json: { errors: use_case.error_messages }
		end
	end

private
	def new_medicine_params
		@_medicine_params ||= MedicineParameterSanitizer.new(params)
	end
	
end