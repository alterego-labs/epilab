class API::V1::Devise::RegistrationsController < Devise::RegistrationsController
  skip_before_filter :verify_authenticity_token
  
  prepend_before_filter :authenticate_user_from_token!, only: [:update]

  respond_to :json
  
  def create
    self.resource = User.new user_params
    resource.save ? signed_up : failed(401)
  end

  def update
    result = params[:user] ? resource.send(update_type, user_params) : true
    result ? updated : failed(422)
  end

protected

  def update_type
    user_params[:password].blank? ? "update_without_password" : "update_with_password"
  end

  def user_params
  	@_user_parameter_sanitizer ||= UserParameterSanitizer.new(params).user_params if params[:user]
  end

  def signed_up
    sign_in :user, resource, store: false
    return render json: { access_token: resource.authentication_token, user: UserSerializer.new(resource, root: false) }
  end

  def updated
    return render json: resource
  end

  def failed(status_code)
    return render status: status_code, json: { errors: resource.errors }
  end

  def failure(message = nil)
    return render status: 401, json: { error: message || TranslationHelper.not_authenticated }
  end

private

  def authenticate_scope!
    self.resource = send(:"current_#{resource_name}")
    failure(TranslationHelper.unauthenticated) unless resource
  end

end