class API::V1::Devise::SessionsController < Devise::SessionsController
	skip_before_filter :verify_authenticity_token
	skip_before_filter :require_no_authentication
	skip_before_filter :verify_signed_out_user

	before_filter :authenticate_user_from_token!, only: [:destroy]

	respond_to :json

	def create
		resource = warden.authenticate! auth_options
		DeviceTokens::Store.new(device_token: params[:device_token], user_id: current_user.id).execute if params[:device_token]
		render json: { access_token: current_user.authentication_token, user: UserSerializer.new(current_user, root: false) }
	end

	def destroy
		resource = warden.authenticate! auth_options
		current_user.set_authentication_token nil
		render json: { success: true }
	end

	def failure(message = nil)
    return render status: 401, json: { error: message || TranslationHelper.not_authenticated }
  end

protected
	def auth_options
		{ scope: resource_name, recall: "#{controller_path}#failure", store: false }
	end

	def authenticate_user_from_token!
		super
		failure(TranslationHelper.unauthenticated) unless current_user
	end

end