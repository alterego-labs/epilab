class API::V1::Devise::PasswordsController < Devise::PasswordsController
	
	def create
		self.resource = resource_class.send_reset_password_instructions(resource_params)
		if successfully_sent?(resource)
			render status: 200, json: { success: true }
    else
      render status: 422, json: { error: "Пользователя с указанным email-адресом не было найдено" }
    end
	end

end