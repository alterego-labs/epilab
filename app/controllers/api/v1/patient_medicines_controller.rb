class API::V1::PatientMedicinesController < ApiController
	authorize_resource 
	load_resource except: :create
	load_and_authorize_resource :patient, class: "User"

	def create
		use_case = ::Patients::Medicines::Create.new(patient_id: params[:patient_id], params: medicine_params)
		if use_case.valid?
			render status: 200, json: use_case.execute
		else
			render status: 422, json: use_case.error_messages
		end
	end

	def update
		use_case = ::Patients::Medicines::Update.new(medicine_id: params[:id], params: medicine_params)
		@medicine = use_case.execute
		if use_case.valid?
			render status: 200, json: @medicine
		else
			render status: 422, json: {
				errors: use_case.error_messages
			}
		end
		# has_errors? ? respond_with(@medicine) : render(status: 200, json: @medicine)
	end

	def destroy
		@patient_medicine.finish!
		render status: 200, json: {}
	end

	def index
		render status: 200, json: @patient_medicines		
	end

private
	def medicine_params
		@_medicine_params ||= PatientMedicineParameterSanitizer.new params if params[:patient_medicine]
	end

	def has_errors?
		@medicine.respond_to?(:errors) && !@medicine.errors.empty?
	end
end