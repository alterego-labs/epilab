class API::V1::MedicationsController < ApiController
	load_and_authorize_resource :patient, class: "User"

	def graphic
		if params[:dx_chart].present?
			medications = Api::Medications::Statistic.new(patient_id: params[:patient_id], period_type: params[:period], states: [:taken], type: "_dxchart").execute
		else
			medications = Rails.cache.fetch(["medications_graphic", params[:patient_id], params[:interval], params[:count], params[:from], @patient.updated_at], expires_in: 1.day) do
				Api::Medications::GroupedStatistic.new(patient_id: params[:patient_id], states: [:taken], interval: params[:interval], count: params[:count], from: params[:from]).execute
			end
		end
		render status: 200, json: medications
	end

	def today
		medications = MedicationQuery.today_for @patient.id
		render status: 200, json: medications
	end

	def taken
		medication = change_state_to :taken
		render status: 200, json: medication
	end

	def rejected
		medication = change_state_to :rejected
		render status: 200, json: medication
	end

	def rejects
		use_case = Rails.cache.fetch(["medications_rejects", params[:patient_id], params[:interval], params[:count], params[:from], @patient.updated_at], expires_in: 1.day) do 
			Api::Medications::Rejects.new(patient_id: params[:patient_id], interval: params[:interval], count: params[:count], from: params[:from]).execute
		end
		render status: 200, json: use_case
	end

private
	def change_state_to(state = :scheduled)
		::Medications::ChangeState.new(medication_id: params[:id], state: state).execute
	end
end