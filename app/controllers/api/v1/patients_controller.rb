class API::V1::PatientsController < ApiController
  load_and_authorize_resource :patient, class: "User"

  def show
    render json: @patient
  end

  def calendar
    use_case = Api::Patients::CalendarEvents.new(patient_id: params[:id])
    render status: 200, json: { dates: use_case.dates, images: use_case.images }
  	
   #  render json: {
  	# 	dates: (Date.today-7.days..Date.today).to_a,
  	# 	images: {
  	# 		"#{Date.yesterday}" => "http://www.pharmaceutical-int.com/upload/image_files/news/289_positive-ruling-on-epileptic-drug-potiga_content_Epileptic_Drugs.jpg/",
  	# 		"#{Date.today}" => "http://www.pharmaceutical-int.com/upload/image_files/featured/malaria-immunity-trials.jpg",
  	# 	}
  	# }
  end

  def info
    medications = Medication.by_patient(@patient.id).by_period date.beginning_of_day, date.end_of_day
    seizures = Seizure.by_patient(@patient.id).by_period date.beginning_of_day, date.end_of_day
    render status: 200, json: { 
      medications: ActiveModel::ArraySerializer.new(medications, each_serializer: MedicationSerializer), 
      seizures: ActiveModel::ArraySerializer.new(seizures, each_serializer: SeizureSerializer),
      date: date
    }
  end
  
  def report

    #ActionMailer::Base.mail(:from => "info@epilab.ru", :to => @patient.email, :subject => 'Эпилаб - годовой отчет').deliver_now
    #API::V1::Devise::UserMailerController.create_email
    UserMailer.welcome_email(@patient.email).deliver
      render status: 200, json: {success:1}



  end

  def graphics
    # seizures = Rails.cache.fetch(["seizures_graphic", params[:id], params[:interval], params[:count], params[:from], @patient.updated_at], expires_in: 1.day) do 
    #   Api::Seizures::GroupedStatistic.new(patient_id: params[:id], interval: params[:interval], count: params[:count], from: params[:from]).execute
    # end
    # medications = Rails.cache.fetch(["medications_graphic", params[:id], params[:interval], params[:count], params[:from], @patient.updated_at], expires_in: 1.day) do
    #   Api::Medications::GroupedStatistic.new(patient_id: params[:id], states: [:taken], interval: params[:interval], count: params[:count], from: params[:from]).execute
    # end
    # rejects = Rails.cache.fetch(["medications_rejects", params[:id], params[:interval], params[:count], params[:from], @patient.updated_at], expires_in: 1.day) do 
    #   Api::Medications::Rejects.new(patient_id: params[:id], interval: params[:interval], count: params[:count], from: params[:from]).execute
    # end
    seizures    = Api::Seizures::GroupedStatistic.new(patient_id: params[:id], interval: params[:interval], count: params[:count], from: params[:from]).execute
    medications = Api::Medications::GroupedStatistic.new(patient_id: params[:id], states: [:taken], interval: params[:interval], count: params[:count], from: params[:from]).execute
    rejects     = Api::Medications::Rejects.new(patient_id: params[:id], interval: params[:interval], count: params[:count], from: params[:from]).execute
    average_duration = Api::Seizures::AverageDuration.new(patient_id: params[:id], interval: params[:interval], count: params[:count], from: params[:from]).execute
    render status: 200, json: { 
      seizures: seizures,
      medications: medications,
      rejects: rejects,
      average_duration: average_duration
    }
  end

private
  def date
    params[:date] ? Date.parse(params[:date]) : Time.zone.today
  end
end
