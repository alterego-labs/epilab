class API::V1::DictionaryController < ApiController
	load_and_authorize_resource class: DictionaryItem

	def index
		resources = DictionaryItemQuery.fetch_by_type params[:bookmark]
		render json: { items: resources.map(&:title) }
	end

end