class MedicinesController < DashboardController
	self.responder = PatientMedicineResponder
	respond_to :html

	def index
		@medicines = current_user.medicines.includes(:medicine)
	end

	def new
		@medicine = current_user.medicines.new
	end

	def create
		@medicine = Patients::Medicines::Create.new(patient_id: current_user.id, params: medicine_params).execute
		respond_with @medicine
	end

	def destroy
		medicine = current_user.medicines.find params[:id]
		medicine.finish! if medicine
		respond_with medicine
	end

	def edit
		@medicine = PatientMedicine.find params[:id]
	end

	def update
		use_case = Patients::Medicines::Update.new(medicine_id: params[:id], params: medicine_params)
		@medicine = use_case.execute
		if use_case.valid?
			redirect_to patient_medicines_path
		else
			render :edit
		end
	end

private
	def medicine_params
		@medicine_params ||= PatientMedicineParameterSanitizer.new params
	end
end
