class ConsultationsController < ApplicationController
	self.responder = ConsultationResponder

	respond_to :html

	def show
		Delayed::Job.enqueue ReadMessagesJob.new(dialog.id, current_user.id) if dialog.unread_by_patient > 0
		@messages = current_user.consultations
	end

	def create
		@question = Consultancy::AddingMessage.new(params: consultation_params, patient_id: current_user.id).execute
		@messages = current_user.consultations
		respond_with @question
	end

private
	def consultation_params
		@_consultation_params ||= ConsultationParameterSanitizer.new params if params[:consultation]
	end

	def dialog
		@_dialog ||= DialogQuery.fetch_by_patient_id(current_user.id)
	end
end