class SeizuresController < ApplicationController
	self.responder = SeizureResponder
	respond_to :html

	before_filter :fetch_seizure, only: [:edit, :update]
	before_filter :fetch_dictionaries, only: [:edit, :update, :new, :create]

	def new
		@seizure = Seizure.new happened_at: DateTime.now.in_time_zone('Moscow')
	end

	def create
		@seizure = Patients::RecordSeizure.new(patient_id: current_user.id, params: seizure_params).execute
		respond_with @seizure
	end

	def edit
	end

	def update
		@seizure.update params.require(:seizure).permit!
		if @seizure.valid?
			redirect_to patient_root_path, notice: "Приступ был успешно обновлен!"
		else
			render :edit
		end
	end

private
	def seizure_params
		@_seizure_params ||= SeizureParameterSanitizer.new params
	end

	def fetch_seizure
		@seizure = Seizure.find params[:id]
	end

	def fetch_dictionaries
		@types = DictionaryItemQuery.fetch_by_type("seizure_type").map(&:title)
		@reasons = DictionaryItemQuery.fetch_by_type("seizure_reason").map(&:title)
	end
end