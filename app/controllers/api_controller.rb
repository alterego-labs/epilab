class ApiController < ApplicationController
	respond_to :json

	prepend_before_filter :disable_devise_trackable
	before_filter :authenticate_user_from_token!

	# rescue_from Exception, with: :error_500

  rescue_from ActionController::RoutingError,
    ActionController::UnknownController,
    ::AbstractController::ActionNotFound,
    ActiveRecord::RecordNotFound, with: lambda {|exception| error_404(exception); } 

	rescue_from CanCan::AccessDenied do |exception|
		render status: 401, json: { error: exception.message }
	end

  def error_404(exception)
  	render status: 404, json: { error: "Not found" }
  end

  def error_500
  	render status: 500, json: { error: "We're sorry, but something went wrong" }
  end

protected
	def disable_devise_trackable
    request.env["devise.skip_trackable"] = true
  end
end
