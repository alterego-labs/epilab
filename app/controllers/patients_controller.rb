class PatientsController < DashboardController
	self.responder = PatientResponder
	respond_to :html

	def today
		@medications = MedicationQuery.today_for current_user.id
		@seizures		 = SeizureQuery.today_for current_user.id
	end

	def statistic
	end

	def edit
		@patient = current_user
	end

	def update
		@patient = Patients::UpdateProfile.new(id: current_user.id, params: patient_params).execute
		respond_with @patient
	end

private
	def patient_params
		UserParameterSanitizer.new params
	end
end