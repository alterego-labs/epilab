class DashboardController < ApplicationController
	before_filter :authorize

private
	def authorize
		current_user ? check_for_patient : access_denied
	end

	def check_for_patient
		redirect_to admin_root_path if current_user.admin?
	end

	def access_denied
		redirect_to root_path
	end
end