class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  layout "mobile", only: :about
  layout "year_report", only: :year_report_statistic
  # TODO: refactor it
  def welcome
  	redirect_to redirect_path
  end

  def about
    @info = Clinic.first
  end

  def year_report_statistic

  end

protected
  def devise_parameter_sanitizer
    if resource_class == User
      User::ParameterSanitizer.new(User, :user, params)
    else
      super
    end
  end

private
	# TODO: refactor it
	def redirect_path
		user_signed_in? ? dashboard_path : new_user_session_path
	end

  def dashboard_path
    (current_user && current_user.admin?) ? admin_root_path : patient_root_path
  end

  def authenticate_user_from_token!
    user = params[:access_token] && UserQuery.find_by_authentication_token(params[:access_token])
    sign_in user, store: false if user
  end

end