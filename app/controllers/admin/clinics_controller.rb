class Admin::ClinicsController < AdminController
	before_filter :find_clinic

	respond_to :html
	self.responder = Admin::ClinicResponder

	def show
		@tab = active_tab
		@doctors  = UserQuery.doctors
	end

	def update
		@clinic.update clinic_params
		respond_with @clinic
	end

private
	def active_tab
		params[:tab] || GlobalConstants::DEFAULT_CLINIC_TAB
	end

	def find_clinic
		@clinic = Clinic.first	
	end

	def clinic_params
		ClinicParameterSanitizer.new(params).clinic_params
	end
end