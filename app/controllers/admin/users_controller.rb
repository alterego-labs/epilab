class Admin::UsersController < ApplicationController
	load_and_authorize_resource

	self.responder = Admin::UserResponder
	respond_to :html

	def index
		@users = User.all
	end

	def destroy
		@user.destroy
		respond_with @user
	end

	def update
		resource = Patients::UpdateProfile.new(id: @user.id, params: user_params).execute
		respond_with resource
	end

	def new; end

	def create; end

private
	def user_params
		UserParameterSanitizer.new params
	end

end
