class Admin::ConsultationsController < ApplicationController

	self.responder = Admin::ConsultationResponder
	respond_to :html

	def create
		@question = Consultancy::AddingMessage.new(params: consultation_params, patient_id: dialog.patient_id, author_id: current_user.id).execute
		respond_with @question
	end

private
	def consultation_params
		@_consultation_params ||= ConsultationParameterSanitizer.new params if params[:consultation]
	end

	def dialog
		@_dialog ||= Dialog.find params[:dialog_id]
	end
end