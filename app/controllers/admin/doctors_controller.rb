class Admin::DoctorsController < AdminController
	authorize_resource

	self.responder = Admin::DoctorResponder
	respond_to :html

	before_filter :find_doctor, only: [:destroy, :edit]
	
	def index
		@doctors = Doctor.all
	end

	def new
		@doctor = Doctor.new
		respond_with @doctor
	end

	def create
		@doctor = Doctors::Employ.new(params: doctor_params).execute
		respond_with @doctor
	end

	def destroy
		@doctor.destroy
		respond_with @doctor
	end

	def sort
		Sortable.new(object: :doctor, id: params[:object][:id], position: params[:object][:position]).execute
		render nothing: true
	end

	def update
		@doctor = Doctors::UpdateProfile.new(id: params[:id], params: doctor_params).execute
		respond_with @doctor
	end

private
	def doctor_params
		@_doctors_params ||= DoctorParameterSanitizer.new params
	end

	def find_doctor
		@doctor ||= Doctor.find params[:id]
	end
end
