class Admin::DictionariesController < ApplicationController
	load_and_authorize_resource class: DictionaryItem, param_method: :item_params

	before_filter :checking_class_name, except: [:sort, :edit, :update, :destroy]

	self.responder = Admin::DictionaryItemResponder
	respond_to :html

	def index
		@items = DictionaryItemQuery.fetch_by_type params[:bookmark]
	end

	def create
		@resource = DictionaryItem.create item_params
		respond_with @resource
	end

	def new
		@resource = DictionaryItem.new type: params[:bookmark]
	end

	def update
		@dictionary.update_attributes item_params
		respond_with @dictionary
	end

	def sort
		Sortable.new(object: :dictionary_item, id: params[:object][:id], position: params[:object][:position]).execute
		render nothing: true
	end

	def destroy
		@dictionary.destroy
		respond_with @dictionary
	end

private
	def object_class_exist?
		GlobalConstants::DICTIONARY_ITEM_TYPES.include? params[:bookmark]
	end

	def checking_class_name
		redirect_to admin_dictionaries_path(bookmark: GlobalConstants::DICTIONARY_ITEM_TYPES.first) unless object_class_exist?
	end

	def item_params
		params.require(:dictionary_item).permit!
	end
end