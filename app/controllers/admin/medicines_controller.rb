class Admin::MedicinesController < AdminController
  authorize_resource

	self.responder = Admin::MedicineResponder
	respond_to :html

	before_filter :find_medicine, only: [:destroy]

  def index
  	@medicines = Medicine.system
  end

  def new
  	@medicine = Medicine.new
  end

  def create
		@medicine = Medicines::Adding.new(params: new_medicine_params).execute
		respond_with @medicine
  end

  def destroy
  	@medicine.destroy
  	respond_with @medicine
  end

  def sort
    Sortable.new(object: :medicine, id: params[:object][:id], position: params[:object][:position]).execute
    render nothing: true
  end

private
	def new_medicine_params
		@_medicine_params ||= MedicineParameterSanitizer.new(params)
	end

	def find_medicine
		@medicine ||= Medicine.find params[:id]
	end
end