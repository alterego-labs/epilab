class Admin::DialogsController < ApplicationController
	load_and_authorize_resource

	def index
		@dialogs = Dialog.joins(:patient).all
	end

	def show
		Delayed::Job.enqueue ReadMessagesJob.new(@dialog.id, current_user.id) if @dialog.unread_by_doctor > 0
		@messages = @dialog.consultations.includes(:author)
	end

private
	def consultation_params
		@_consultation_params ||= ConsultationParameterSanitizer.new params if params[:consultation]
	end
end
