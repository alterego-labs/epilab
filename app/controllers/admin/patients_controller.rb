class Admin::PatientsController < AdminController
	# self.responder = Admin::PatientsResponder

	respond_to :html, :json

  def index
  	authorize! :show, User
  	@patients = UserQuery.patients
  end

  def search
		authorize! :search, User
		patients = UserQuery.fetch_by_name params[:query]
		render status: 200, json: patients, each_serializer: PatientSerializer, root: false
  end

  def show
    @patient = UserQuery.find_patient_by_id params[:id]
  end
end
