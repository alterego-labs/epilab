class User::ParameterSanitizer < Devise::ParameterSanitizer

  def sign_up
    default_params.permit :first_name, :last_name, :birthday, :email, :password, :sex, :is_watcher
  end

  def account_update
    default_params.permit!
  end

end