class MedicationsController < DashboardController
	respond_to :js

	def taken
		@medication = change_state_to :taken
		render :changed
	end

	def rejected
		@medication = change_state_to :rejected
		render :changed
	end

private
	def change_state_to(state = :scheduled)
		Medications::ChangeState.new(medication_id: params[:id], state: state).execute
	end
end