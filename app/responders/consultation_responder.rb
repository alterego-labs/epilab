class ConsultationResponder < AlterMvc::BasicResponder
	def create_html
		unless has_errors?
			redirect_to controller.patient_consultation_path
		else
			render :show
		end
	end
end