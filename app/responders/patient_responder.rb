class PatientResponder < AlterMvc::BasicResponder
	def update_html
		unless resource.errors.any?
			set_flash_notice TranslationHelper.success_action(:patient, :update)
			controller.sign_in :user, resource, bypass: true
			redirect_to controller.edit_patient_path
		else
			render :edit
		end
	end
end