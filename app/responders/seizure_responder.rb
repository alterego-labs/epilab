class SeizureResponder < AlterMvc::BasicResponder
	def create_html
		unless has_errors?
			set_flash_notice TranslationHelper.success_action(:seizure, :create, title: resource.type)
			redirect_to controller.patient_root_path
		else
			render :new
		end
	end
end