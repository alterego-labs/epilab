module Admin
	class MedicineResponder < AlterMvc::BasicResponder
		def create_html
			unless has_errors?
				set_flash_notice I18n.t("flash.medicine.create.success", title: resource.title)
				redirect_to controller.admin_medicines_path
			else
				render :new
			end
		end

		def destroy_html
			set_flash_notice I18n.t("flash.medicine.destroy.success", title: resource.title)
			redirect_to :back
		end

	end
end