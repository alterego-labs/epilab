module Admin
	class UserResponder < AlterMvc::BasicResponder
		def destroy_html
			set_flash_notice TranslationHelper.success_action(:user, :destroy, title: resource.full_name)
			redirect_to :back
		end

		def update_html
			unless has_errors?
				set_flash_notice TranslationHelper.success_action(:user, :update, title: resource.full_name)
				redirect_to controller.admin_users_path	
			else
				render :edit
			end
		end

	end
end