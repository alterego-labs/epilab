module Admin
	class DoctorResponder < AlterMvc::BasicResponder

		def create_html
			unless has_errors?
				set_flash_notice I18n.t("flash.doctor.create.success", title: resource.full_name)
				redirect_to controller.admin_doctors_path
			else
				render :new
			end
		end

		def destroy_html
			set_flash_notice I18n.t("flash.doctor.destroy.success", title: resource.full_name)
			redirect_to :back
		end

		def update_html
			unless has_errors?
				set_flash_notice I18n.t("flash.doctor.update.success", title: resource.full_name)
				redirect_to controller.admin_doctors_path	
			else
				render :edit
			end
		end

	end
end