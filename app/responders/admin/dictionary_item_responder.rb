module Admin
	class DictionaryItemResponder < AlterMvc::BasicResponder
		def create_html
			unless has_errors?
				set_flash_notice TranslationHelper.success_action(:dictionary_item, :create, title: resource.title)
				redirect_to controller.admin_dictionaries_path(bookmark: resource.type)
			else
				render :new
			end
		end

		def update_html
			unless has_errors?
				set_flash_notice TranslationHelper.success_action(:dictionary_item, :update, title: resource.title)
				redirect_to controller.admin_dictionaries_path(bookmark: resource.type)
			else
				render :edit
			end
		end

		def destroy_html
			set_flash_notice TranslationHelper.success_action(:dictionary_item, :destroy, title: resource.title)	
			redirect_to controller.admin_dictionaries_path(bookmark: resource.type)
		end
	end
end