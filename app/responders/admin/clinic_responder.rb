module Admin
	class ClinicResponder < AlterMvc::BasicResponder
		def update_html
			set_flash_notice I18n.t "flash.clinic.update.success"
			redirect_to controller.admin_clinic_path tab: controller.send(:active_tab)
		end
	end
end