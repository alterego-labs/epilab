class Admin::ConsultationResponder < AlterMvc::BasicResponder
	def create_html
		unless has_errors?
			redirect_to controller.admin_dialog_path(resource.dialog_id)
		else
			render :show
		end
	end
end