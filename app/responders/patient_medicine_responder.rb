class PatientMedicineResponder < AlterMvc::BasicResponder
	def create_html
		unless has_errors?
			set_flash_notice TranslationHelper.success_action(:patient_medicine, :create, title: resource.draw_title)
			redirect_to controller.patient_medicines_path
		else
			render :new
		end
	end

	def destroy_html
		unless has_errors?
			set_flash_notice TranslationHelper.success_action(:patient_medicine, :destroy, title: resource.draw_title)
		end
		redirect_to :back
	end
end