class ConsultationValidator < AlterMvc::BasicValidator
	validates_presence_of :text, :patient_id, :author_id, :direction
end