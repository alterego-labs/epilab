class DoctorValidator < AlterMvc::BasicValidator
	validates :first_name, :last_name, presence: true
end