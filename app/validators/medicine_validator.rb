class MedicineValidator < AlterMvc::BasicValidator
	validates_presence_of :title
	validate :is_unique_title

private
	def is_unique_title
		errors.add :title, TranslationHelper.default_error_message(:unique) if MedicineQuery.find_by_title_for_patient(self.title, self.patient_id).any?
	end
end