class PatientMedicineValidator < AlterMvc::BasicValidator
	validates_presence_of :user_id, :medicine_id, :beginning_at, :periodicity
	validate :reception_exists?
	validate :medicine_exists?

private
	def reception_exists?
		errors.add :reception, "Дозировка не может быть пустой!" if !self.reception || self.reception.empty?
	end

	def medicine_exists?
		errors.add :medicine_id, "уже существует в списке приема" if self.user.medicines.by_medicine_id(medicine_id).any?
	end
end