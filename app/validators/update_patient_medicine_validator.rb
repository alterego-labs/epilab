class UpdatePatientMedicineValidator < AlterMvc::BasicValidator
	validates_presence_of :user_id, :medicine_id, :beginning_at, :periodicity
	validate :reception_exists?

private
	def reception_exists?
		errors.add :reception, "Reception is invalid!" if self.reception.empty?
	end

end