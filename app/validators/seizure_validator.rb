class SeizureValidator < AlterMvc::BasicValidator
	include Paperclip::Validators
	
	validates_presence_of :type, :reason, :duration, :happened_at
	validates_with AttachmentContentTypeValidator, attributes: :video, content_type: /\Avideo/
	validates_numericality_of :duration
end