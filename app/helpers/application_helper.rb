module ApplicationHelper
	def active_link_to(text, path, options={})
    if /^#{url_for(path)}/ =~ request.path
      options[:class] = "#{options[:class]} active".strip
    end
    link_to text, path, options
	end

	def active_nav_link_to(text = nil, path = {}, options = {}, &block)
		text, path, options = block, text, path if block_given?
		
		if url_for(path) == request.path
			options[:class] = "#{options[:class]} active".strip
		end

		options[:href] = path

		link = content_tag :a, text, options, &block

		content_tag :li, link, options
	end

	def render_navbar
		render partial: "layouts/#{navbar_partial}"
	end

	def link_to_if_with_block condition, options, html_options={}, &block
		if condition
			link_to options, html_options, &block
		else
			capture &block
		end
	end

  def add_host_prefix(url)
    "#{ActionController::Base.asset_host}#{url}"
  end

  def site_user
  	current_user || User.new
  end
private
	def navbar_partial
		current_user.admin? ? "admin_navbar" : "patient_navbar"
	end
end
