module FlatFlashHelper
	ALERT_TYPES = [:danger, :info, :success, :warning] unless const_defined?(:ALERT_TYPES)

	def flat_flash(options = {})
		flash_messages = []
		flash.each do |type, message|
			# Skip empty messages, e.g. for devise messages set to nothing in a locale file.
			next if message.blank?

			type = type.to_sym
			type = :success if type.to_s == :notice.to_s
			type = :danger   if type.to_s == :alert.to_s
			next unless ALERT_TYPES.include?(type)

			Array(message).each do |msg|
				container = content_tag(:div, msg, class: "container")
				text = content_tag(:div, container, class: "dialog dialog-#{type} #{options[:class]}")
				flash_messages << text if msg
			end
		end
		flash_messages.join("\n").html_safe
	end

	def custom_message(message, options = {})
		message = content_tag :div, message, class: "container"
		container = content_tag :div, message, class: "dialog #{options[:class]}"
		row = content_tag :div, container, class: "row"
		row.html_safe
	end
end