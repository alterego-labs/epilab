class ClinicSerializer < ActiveModel::Serializer
  attributes :about, :coordinates, :address, :phone, :email, :site
end
