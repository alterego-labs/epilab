class SeizureSerializer < ActiveModel::Serializer
  include ApplicationHelper

  attributes :id, :type, :time, :duration, :video, :comment, :reason, :video_thumb

  def time
  	object.numeric_time
  end

  def duration
  	object.numeric_duration
  end

  def video
  	object.video_url
  end

  def video_thumb
    add_host_prefix object.video.url(:thumb)
  end
end