class DoctorSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :about, :photo, :job

  def photo
  	object.photo_url
  end
end
