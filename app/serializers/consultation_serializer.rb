class ConsultationSerializer < ActiveModel::Serializer
  attributes :id, :text, :author, :direction, :time

  def author
  	object.draw_author_name
  end

  def time
  	object.created_at.to_i
  end
end