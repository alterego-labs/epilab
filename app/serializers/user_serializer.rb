class UserSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :email, :photo_url, :birthday, :sex, :role, :is_watcher

  def photo_url
  	return nil if object.photo_file_name.nil?
    absolute_uri.to_s
  end

 private

  def absolute_uri
    URI.join(ActionController::Base.asset_host, object.photo.url(:medium))
  end
end
