class MedicationSerializer < ActiveModel::Serializer
	attributes :id, :title, :time, :dosage, :state

	def time
		object.numeric_time
	end

	def title
		object.draw_title
	end
end
