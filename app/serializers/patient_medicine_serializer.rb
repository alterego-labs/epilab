class PatientMedicineSerializer < ActiveModel::Serializer
  attributes :id, :title, :reception, :beginning_at, :remind, :periodicity

  def title
  	object.draw_title
  end

end