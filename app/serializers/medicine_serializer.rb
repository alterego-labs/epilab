class MedicineSerializer < ActiveModel::Serializer
  attributes :id, :title
end
