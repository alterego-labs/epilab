class PatientSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :email, :photo, :url

  def photo
  	object.photo_url
  end

  def url
  	admin_patient_url object
  end
end