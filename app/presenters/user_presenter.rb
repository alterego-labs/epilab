class UserPresenter < AlterMvc::BasicPresenter
	def full_name
		[last_name, first_name].join(" ")
	end

	def initial_name
		"#{last_name} #{first_name[0]}."
	end

	def sex_letter
		draw_sex && draw_sex[0] || nil
	end

	def draw_sex
		model.sex && I18n.t(model.sex, scope: "dictionary.sex") || nil
	end

	def draw_role
		TranslationHelper.role model.role
	end

	def role_label_class
		Users::Constants::ROLES_CLASSES[model.role]
	end

end