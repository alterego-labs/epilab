class PatientMedicinePresenter < AlterMvc::BasicPresenter
	def draw_time
		time = Time.parse(model.time)
    time.hour * 3600 + time.min * 60
	end

	def draw_title
		return unless model.medicine
		model.medicine.title
	end

	def draw_beginning_at
		I18n.l model.beginning_at, format: "%d %b %y"
	end

	def draw_next_medication_date
		I18n.l service_object.next_medication_date, format: "%d %b %y"
	end

	def draw_periodicity
		TranslationHelper.periodicity model.periodicity
	end

	def draw_dosage
		"#{model.dosage} мл"
	end
end