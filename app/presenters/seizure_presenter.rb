class SeizurePresenter < AlterMvc::BasicPresenter
	def draw_time
		model.happened_at.to_s :time
	end

	def draw_date
		Russian::strftime model.happened_at, "%d %b %y"
	end

	def draw_duration
		model.duration.to_i		
	end

end