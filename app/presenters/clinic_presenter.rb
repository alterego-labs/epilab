class ClinicPresenter < AlterMvc::BasicPresenter
	def longitude
		coordinates[0]
	end

	def latitude
		coordinates[1]
	end

	def coordinates
		model.coordinates.split(",")
	end
end