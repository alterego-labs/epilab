class ConsultationPresenter < AlterMvc::BasicPresenter
	def draw_author_name
		model.author.full_name if model.author
	end

	def draw_time
		I18n.l model.created_at, format: "%d %b %Y %H:%M"
	end
end