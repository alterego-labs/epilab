class MedicationPresenter < AlterMvc::BasicPresenter
	def draw_time
		model.time.to_s :time
	end

	def draw_date
		Russian::strftime model.time, "%d %b %y"
	end

	def draw_title
		"#{model.medicine.title}"
	end

	def draw_title_with_dosage
		"#{model.medicine.title} #{model.dosage}мг"
	end
end