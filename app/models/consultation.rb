class Consultation < ActiveRecord::Base
	include AlterMvc::Concerns::Querable
	include AlterMvc::Concerns::Presentable
	include AlterMvc::Concerns::ServiceObjectable
	
	enum direction: { income: 1, outcome: 0 }

	default_scope { order created_at: :desc }
	scope :unread, ->{ where(is_read: false) }

  belongs_to :patient, class_name: User, foreign_key: :patient_id
  belongs_to :author, class_name: User, foreign_key: :author_id
  belongs_to :dialog, counter_cache: :messages_count, touch: true
end
