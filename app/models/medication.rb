class Medication < ActiveRecord::Base
	include AlterMvc::Concerns::Querable
	include AlterMvc::Concerns::Presentable
	include AlterMvc::Concerns::ServiceObjectable

	default_scope { order time: :asc }

	scope :by_state, ->(states) { includes(:medicine).where state: states }
	scope :by_period, ->(start_at, end_at) { where("time >= :start_at AND time <= :end_at", start_at: start_at.beginning_of_day, end_at: end_at.end_of_day) }
  scope :by_date, ->(date) { by_period date.beginning_of_day, date.end_of_day }
  scope :by_patient, ->(patient_id) { where patient_id: patient_id }
  scope :by_time_range, ->(s, e) { where "time > (?) AND time < (?)", s, e }
  scope :for_remind, ->{ joins(:patient_medicine).where(patient_medicines: { remind: true } ) }

	belongs_to :patient, class_name: "User", foreign_key: :patient_id, touch: true
  belongs_to :patient_medicine
  belongs_to :medicine
end
# == Schema Information
#
# Table name: medications
#
#  id                  :integer          not null, primary key
#  time                :datetime
#  created_at          :datetime
#  updated_at          :datetime
#  state               :string(255)      default("scheduled"), not null
#  medicine_id         :integer
#  dosage              :integer
#  patient_id          :integer
#  patient_medicine_id :integer
#

#
