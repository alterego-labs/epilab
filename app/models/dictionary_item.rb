# == Schema Information
#
# Table name: dictionary_items
#
#  id         :integer          not null, primary key
#  type       :string(255)
#  title      :string(255)
#  position   :integer          default(0)
#  created_at :datetime
#  updated_at :datetime
#

class DictionaryItem < ActiveRecord::Base
	self.inheritance_column = nil
	include AlterMvc::Concerns::Presentable

	validates_presence_of :title, :type
	validates :type, inclusion: GlobalConstants::DICTIONARY_ITEM_TYPES
	validates_uniqueness_of :title

	default_scope { order position: :desc }

	acts_as_positioned by_field: :type
end
