class PatientMedicine < ActiveRecord::Base
	include AlterMvc::Concerns::Querable
	include AlterMvc::Concerns::Presentable
	include AlterMvc::Concerns::ServiceObjectable

	enum periodicity: { "everyday" => 1, "day" => 2, "weekly" => 3, "fortnightly" => 4, "monthly" => 5 }

	serialize :reception, JSON

	scope :by_medicine_id, ->(id){ where medicine_id: id }
	scope :active, ->{ where finished_at: nil }
	scope :finished, ->{ where "finished_at != NULL" }
	default_scope { active }

	has_many :medications

	belongs_to :user, counter_cache: :medicines_count, touch: true
	belongs_to :medicine

	def reception=(value)
		self[:reception] = ReceptionJsonSerializer.parse value
	end

	def periodicity=(value)
		if PatientMedicine.periodicities.keys.include? value
			self[:periodicity] = PatientMedicine.periodicities[value]
		else
			self[:periodicity] = value.to_i
		end
	end


end
# == Schema Information
#
# Table name: patient_medicines
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  medicine_id  :integer
#  created_at   :datetime
#  updated_at   :datetime
#  beginning_at :date
#  periodicity  :integer
#  reception    :text
#  remind       :boolean          default(FALSE), not null
#  finished_at  :datetime
#
#