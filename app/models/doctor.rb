class Doctor < ActiveRecord::Base
	# include AlterMvc::Concerns::Querable
	include AlterMvc::Concerns::Presentable
	include AlterMvc::Concerns::ServiceObjectable

	acts_as_positioned under: :member

	default_scope { order position: :asc }

	has_attached_file :photo, styles: { medium: "300x300#", thumb: "160x160#", small: "40x40#" }, default_url: "/images/icons/default.png"
  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/
  validates_presence_of :job
end

# == Schema Information
#
# Table name: doctors
#
#  id                 :integer          not null, primary key
#  first_name         :string(255)
#  last_name          :string(255)
#  about              :text
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  created_at         :datetime
#  updated_at         :datetime
#  position           :integer
#