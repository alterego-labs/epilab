# == Schema Information
#
# Table name: device_tokens
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  device_token :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

class DeviceToken < ActiveRecord::Base
	include AlterMvc::Concerns::Querable
  belongs_to :user

  scope :by_device_token, ->(device_token){ where device_token: device_token }
end
