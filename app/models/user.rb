class User < ActiveRecord::Base
	include AlterMvc::Concerns::Querable
	include AlterMvc::Concerns::Presentable
	include AlterMvc::Concerns::ServiceObjectable
	include Users::Constants

	attr_accessor :current_password

	has_attached_file :photo, styles: { medium: "300x300#", thumb: "160x160#", small: "40x40#" }, default_url: "/images/icons/default.png"
  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/

  default_scope { order id: :asc }

	before_save :ensure_authentication_token

	devise :database_authenticatable, :registerable,
				 :recoverable, :rememberable, :validatable

	has_many :medicines, class_name: PatientMedicine
	has_many :medications, foreign_key: :patient_id
	has_many :seizures, foreign_key: :patient_id
	has_many :consultations, foreign_key: :patient_id
	has_many :device_tokens

	validates :first_name, :last_name, :role, presence: true
	validates :role, inclusion: { in: ROLES }
	validates :sex, inclusion: { in: SEXES }, allow_nil: true
	validates :password, confirmation: true
	validates :birthday, date: true, allow_nil: true

	DETAILS.each {|param| store_accessor :details, param }

	after_initialize do
		self.sex = SEXES.first unless self.sex
		self.role = "patient" unless self.role
		self.is_watcher = false if self.patient? && !self.is_watcher.present?
	end

	def ensure_authentication_token
		self.authentication_token = generate_authentication_token if self.authentication_token.blank?
	end

private
	def generate_authentication_token
		loop do
			token = Devise.friendly_token
			break token unless UserQuery.find_by_authentication_token(token)
		end
	end

end
# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string(255)
#  last_name              :string(255)
#  email                  :string(255)
#  role                   :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  details                :hstore
#  photo_file_name        :string(255)
#  photo_content_type     :string(255)
#  photo_file_size        :integer
#  photo_updated_at       :datetime
#  seizures_count         :integer          default(0), not null
#  authentication_token   :string(255)
#  sign_in_count          :integer
#  medicines_count        :integer          default(0), not null
#

#
