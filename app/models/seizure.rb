class Seizure < ActiveRecord::Base
	self.inheritance_column = nil
	include AlterMvc::Concerns::Querable
	include AlterMvc::Concerns::Presentable
	include AlterMvc::Concerns::ServiceObjectable

  has_attached_file :video, styles: {
    thumb: { geometry: '100x100#', format: 'jpg', time: 1  }
  }, processors: [:transcoder]

  validates_attachment_content_type :video, content_type: /\Avideo/
  validates_presence_of :type, :reason, :duration, :happened_at
  validates_numericality_of :duration

  scope :by_period, ->(start_at, end_at) { where("happened_at >= :start_at AND happened_at <= :end_at", start_at: start_at.beginning_of_day, end_at: end_at.end_of_day) }
  scope :by_date, ->(date) { by_period date.beginning_of_day, date.end_of_day }
  scope :by_patient, ->(patient_id) { where patient_id: patient_id }

  belongs_to :user, foreign_key: :patient_id, counter_cache: true, touch: true

  def clear_video
    video = nil
    save
  end

end
# == Schema Information
#
# Table name: seizures
#
#  id                 :integer          not null, primary key
#  patient_id         :integer
#  type               :string(255)
#  reason             :string(255)
#  duration           :string(255)
#  comment            :text
#  video_file_name    :string(255)
#  video_content_type :string(255)
#  video_file_size    :integer
#  video_updated_at   :datetime
#  created_at         :datetime
#  updated_at         :datetime
#  happened_at        :datetime
#

#
