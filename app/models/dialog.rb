# == Schema Information
#
# Table name: dialogs
#
#  id                :integer          not null, primary key
#  patient_id        :integer
#  messages_count    :integer
#  unread_by_patient :integer          default(0)
#  unread_by_doctor  :integer          default(0)
#  created_at        :datetime
#  updated_at        :datetime
#

class Dialog < ActiveRecord::Base
	include AlterMvc::Concerns::Querable
	include AlterMvc::Concerns::Presentable
	include AlterMvc::Concerns::ServiceObjectable

  belongs_to :patient, class_name: User
  has_many :consultations

  default_scope { order updated_at: :desc }
  scope :by_patient, ->(id) { where patient_id: id }

  after_touch :recalc_unread_messages

private
	def recalc_unread_messages
		self.update_unread_messages_count
	end
end
