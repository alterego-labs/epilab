# == Schema Information
#
# Table name: clinics
#
#  id          :integer          not null, primary key
#  about       :text
#  coordinates :string(255)
#  address     :string(255)
#  phone       :string(255)
#  email       :string(255)
#  site        :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class Clinic < ActiveRecord::Base
	include AlterMvc::Concerns::Presentable
end
