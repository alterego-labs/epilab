class Medicine < ActiveRecord::Base
	include AlterMvc::Concerns::Querable
	include AlterMvc::Concerns::Presentable
	include AlterMvc::Concerns::ServiceObjectable
	acts_as_positioned by_field: :patient_id

	default_scope { order position: :asc }
	scope :for_patient, ->(patient_id){ where "patient_id = :patient_id OR patient_id IS NULL", patient_id: patient_id }
	scope :ordered_by_patient, ->{ reorder "patient_id DESC, position ASC, created_at DESC" }
	scope :system, ->{ where "patient_id IS NULL" }

	has_many :medications
	has_many :patient_medicines

	belongs_to :patient, class_name: "User", primary_key: :patient_id
	
end
# == Schema Information
#
# Table name: medicines
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  created_at :datetime
#  updated_at :datetime
#  position   :integer          default(0), not null
#  patient_id :integer
#

#
