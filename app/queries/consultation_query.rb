class ConsultationQuery < AlterMvc::BasicQuery
	def mark_as_read!(objs)
		objs.update_all is_read: true, updated_at: Time.now
	end

	def fetch_for_patient(patient_id)
		model.where patient_id: patient_id
	end
end