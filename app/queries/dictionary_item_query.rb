class DictionaryItemQuery < AlterMvc::BasicQuery
	def fetch_by_type(bookmark)
		model.where type: bookmark
	end
end