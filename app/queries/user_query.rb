class UserQuery < AlterMvc::BasicQuery
	include Users::Constants

	def admins
		model.where role: "admin"
	end

	def doctors
		model.where role: "doctor"
	end

	def patients
		model.where role: "patient"
	end

	def watchers
		patients.where "details @> 'is_watcher => true'"
	end

	def fetch_by_role(role)
		model.where role: role
	end

	def find_doctor_by_id(id = nil)
		id && doctors.find_by(id: id)
	end

	def find_patient_by_id(id = nil)
		id && patients.find_by(id: id)
	end

	def find_by_authentication_token(token)
		model.find_by authentication_token: token
	end

	def fetch_by_name(name = nil)
		patients.where("last_name ILIKE ?", "#{name}%").limit(5)
	end

	def scheduled!
		model.update latest_scheduled_date: Time.zone.now
	end

	def set_authentication_token(token)
		model.update authentication_token: token
	end

	def unreaded_messages
		model.admin? ? unread_messages_for_admin : unread_messages_for_patient
	end

	def available_medicines
		Medicine.for_patient(model.id).ordered_by_patient
	end

	def taken_medicines_ids
		PatientMedicine.unscoped.where(user_id: model.id).pluck(:medicine_id).uniq
	end

	def taken_medicines
		PatientMedicine.unscoped.where(user_id: model.id).includes(:medicine).map(&:medicine).uniq.compact
	end

	def taken_medicines_count
		PatientMedicine.unscoped.where(user_id: model.id).count
	end

private
	def unread_messages_for_admin
		Dialog.joins(:patient).sum :unread_by_doctor
	end

	def unread_messages_for_patient
		Dialog.joins(:patient).by_patient(model.id).first.try :unread_by_patient
	end
end