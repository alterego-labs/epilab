class MedicationQuery < AlterMvc::BasicQuery
	def today
		model.includes(:medicine).where("time > :start_time AND time < :end_time", 
																		start_time: Time.zone.now.beginning_of_day, 
																		end_time: Time.zone.now.end_of_day).order(time: :asc)
	end

	def today_for(patient_id)
		model.includes(:medicine).by_patient(patient_id).by_period(Time.zone.now.beginning_of_day, Time.zone.now.end_of_day)
	end

	def taken!
		model.update state: :taken
	end

	def rejected!
		model.update state: :rejected
	end

	def fetch_by_states(states)
		model.includes(:medicine).where(state: states)
	end

	def fetch_by_period(period_start, period_end)
		model.includes(:medicine).where("time > :start_time AND time < :end_time", 
																		start_time: period_start, 
																		end_time: period_end)
	end

end