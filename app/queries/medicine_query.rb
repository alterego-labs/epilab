class MedicineQuery < AlterMvc::BasicQuery
	def max_position
		model.maximum :position
	end

	def find_by_title(title)
		model.system.where title: title
	end

	def find_by_title_for_patient(title, patient_id)
		model.for_patient(patient_id).where title: title
	end

	def fetch_by_ids(ids)
		model.where id: ids
	end

	def max_updated_at
		model.maximum :updated_at
	end
end