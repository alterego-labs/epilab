class DialogQuery < AlterMvc::BasicQuery
	def fetch_by_patient_id(patient_id)
		model.by_patient(patient_id).first_or_create
	end

	def update_unread_messages_count
		model.update unread_by_doctor: model.unreaded_by_doctor.count, unread_by_patient: model.unreaded_by_patient.count
	end

	def unreaded_by_doctor
		model.consultations.unread.where("patient_id = author_id")
	end

	def unreaded_by_patient
		model.consultations.unread.where("patient_id <> author_id")
	end

	def last_message
		model.consultations.reorder(created_at: :asc).last
	end

end