class SeizureQuery < AlterMvc::BasicQuery
	def today
		model.where("happened_at >= :start_time AND happened_at <= :end_time",
								start_time: Time.zone.now.beginning_of_day,
								end_time: Time.zone.now.end_of_day)
	end

	def today_for(patient_id)
		model.by_patient(patient_id).by_period(Time.zone.now.beginning_of_day, Time.zone.now.end_of_day)
	end

  def find_for(patient_id, seizure_id)
    model.by_patient(patient_id).where(id: seizure_id).first
  end
end