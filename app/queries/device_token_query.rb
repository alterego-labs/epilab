class DeviceTokenQuery < AlterMvc::BasicQuery
	def fetch_by_token(device_token)
		DeviceToken.by_device_token(device_token).first_or_create
	end

	def set_user(user_id)
		model.update user_id: user_id
	end
end