class PatientMedicineQuery < AlterMvc::BasicQuery
	def finish!
		model.update finished_at: Time.zone.now
	end

	def latest_medication
		model.medications.reorder(time: :desc).first
	end

	def reception_times
		(0..23).to_a.inject([]) do |res, el|
			res << "#{sprintf('%.2d', el)}:00"
			res << "#{sprintf('%.2d', el)}:30"
			res
		end
	end

end