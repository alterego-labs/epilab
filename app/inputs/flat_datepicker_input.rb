class FlatDatepickerInput < SimpleForm::Inputs::Base
	def input
		template.content_tag(:div) do
			template.concat @builder.text_field(attribute_name, html_options)
			template.concat calendar_icon
		end
	end

	def html_options
		input_html_options.merge custom_html_options
	end

	def custom_html_options
		{ class: "form-control flat-datepicker" }
	end

	def calendar_icon
		template.content_tag :span, nil, class: "input-icon fui-calendar"
	end

	# def calendar_button
	# 	template.content_tag(:span, class: "input-group-btn") do
	# 		template.concat calendar_icon
	# 	end
	# end

	# def calendar_icon
	# 	template.content_tag :button, type: "button", class: "btn" do
	# 		template.concat template.content_tag :span, nil, class: "fui-calendar"
	# 	end
	# end
end