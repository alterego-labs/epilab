# app/inputs/image_preview_input.rb
class ImagePreviewInput < SimpleForm::Inputs::FileInput
  def input
    out = template.content_tag :div, class: "fileinput fileinput-new", "data-provides" => "fileinput" do
      thumbnail = ""
      thumbnail << template.content_tag(:div, class: "fileinput-new thumbnail", style: "width: 160px; height: 160px;") do
        template.image_tag object.send(attribute_name).url(:thumb)
      end
      thumbnail << template.content_tag(:div, "", class: "fileinput-preview fileinput-exists thumbnail").html_safe
      thumbnail << template.content_tag(:div) do
        controls = ""
        controls << template.content_tag(:span, class: "btn btn-default btn-block btn-embossed btn-file btn-xs") do
          buttons = ""
          buttons << template.content_tag(:span, class: "fileinput-new") do
            new_button = ""
            new_button << template.content_tag(:span, "", class: "fui-image")
            new_button << "&nbsp;&nbsp;Select image"
            new_button.html_safe
          end
          buttons << template.content_tag(:span, class: "fileinput-exists") do
            exists_button = ""
            exists_button << template.content_tag(:span, "", class: "fui-gear")
            exists_button << "&nbsp;&nbsp;Change"
            exists_button.html_safe
          end
          buttons << @builder.file_field(attribute_name, input_html_options).html_safe
          buttons.html_safe
        end
        controls << template.content_tag(:a, class: "btn btn-default btn-block btn-embossed fileinput-exists btn-xs", "data-dismiss" => "fileinput", href: "#") do
          trash = ""
          trash << template.content_tag(:span, "", class: "fui-trash")
          trash << "&nbsp;&nbsp;Remove"
          trash.html_safe
        end
        controls.html_safe
      end
      thumbnail.html_safe
    end
    out.html_safe
  end
end