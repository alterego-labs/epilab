module Medications
	class ChangeState < AlterMvc::BasicUseCase
		attr_reader :medication_id, :state

		def execute
			medication.state == state.to_s ? clear_state : update_state
			medication
		end

	private
		def medication
			@_medication ||= Medication.find medication_id
		end

		def update_state
			medication.update state: state if state
		end

		def clear_state
			medication.update state: "scheduled"
		end
	end
end