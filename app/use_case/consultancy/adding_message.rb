module Consultancy
	class AddingMessage < AlterMvc::BasicUseCase
		attr_reader :params, :patient_id, :author_id

		def execute
			processing_message
			message
		end

	private
		def processing_message
			message.save if valid?
			::Notifications::Messages.new(message: message).execute
		end

		def valid?
			validation.valid?
		end

		def validation
			@_validation ||= ConsultationValidator.new message
		end

		def message
			@_message ||= dialog.consultations.new message_params
		end

		def dialog
			@_dialog ||= DialogQuery.fetch_by_patient_id patient_id
		end

		def message_params
			params ? params.consultation_params.merge(auto_message_params) : auto_message_params
		end

		def auto_message_params
			{
				patient_id: patient_id,
				author_id: author,
				direction: Consultation.directions[direction]
			}
		end

		def author
			author_id || patient_id
		end

		def direction
			author == patient_id ? :outcome : :income
		end

	end
end