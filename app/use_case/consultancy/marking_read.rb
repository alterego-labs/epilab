module Consultancy
	class MarkingRead < AlterMvc::BasicUseCase
		attr_reader :dialog_id, :reader_id
		def execute
			return unless valid?
			mark_as_read
			recalc_counters
		end

	private
		def valid?
			dialog_id && reader_id && dialog
		end

		def dialog
			@_dialog ||= Dialog.find dialog_id
		end

		def unread_messages
			dialog.consultations.unread.where "author_id != :reader_id", reader_id: reader_id
		end

		def mark_as_read
			unread_messages.update_all is_read: true
		end

		def recalc_counters
			dialog.touch
		end
	end
end