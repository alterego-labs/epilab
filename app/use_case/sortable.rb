class Sortable < AlterMvc::BasicUseCase
	attr_reader :object, :id, :position

	def execute
		return unless valid?
		update
	end

private
	def update
		model.update update_params
	end

	def valid?
		class_name && id && position && model
	end

	def model
		@_object = class_name.find id
	end

	def class_name
		object.to_s.classify.constantize
	end

	def update_params
		{
			position: position
		}
	end
end