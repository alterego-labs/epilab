module Scheduling
	class ProcessMedication < AlterMvc::BasicUseCase
		attr_reader :patient_medicine_id, :date, :force

		def execute
			return unless valid?
			schedule
		end

		def valid?
			patient_medicine && (patient_medicine.should_be_scheduled?(date: datetime) || force? )
		end

	private
		def schedule
			patient_medicine.receptions.each do |reception|
				patient.medications.where(schedule_params(reception["time"], reception["dosage"])).first_or_create
			end
		end

		def patient_medicine
			@_medication ||= PatientMedicine.includes(:user).find_by id: patient_medicine_id
		end

		def patient
			@_patient ||= patient_medicine.user
		end

		def schedule_params(time, dosage)
			{
				time: construct_time(time),
				dosage: dosage.to_i,
				medicine_id: patient_medicine.medicine_id,
				patient_medicine_id: patient_medicine_id
			}
		end

		def construct_time(time)
			Time.zone.parse(datetime.to_s).change time_array(time)
		end

		def datetime
			date ? date : Time.zone.today
		end

		def time_array(time)
			{
				hour: time.split(":")[0],
				min:  time.split(":")[1]
			}
		end

		def force?
			force == true
		end

	end
end