module Scheduling
	class ProcessPatients < AlterMvc::BasicUseCase
	
		def execute
			patients.find_each {|patient| process_medications_for patient }
		end

	private

		def patients
			@_patients ||= UserQuery.patients
		end

		def process_medications_for(patient)
			Patients::ScheduleMedicines.new(patient_id: patient.id, date: Time.zone.now).execute
			patient.scheduled!
		end

	end
end