module Notifications
	class Medications < AlterMvc::BasicUseCase
		
		def execute
			process!
		end

	private

		def time
			@_time ||= Time.zone.now
		end

		def time_range
			{ first: time - 15.minutes, last: time + 11.minutes }
		end

		def medications
			@_medications ||= Medication.for_remind.joins(patient: :device_tokens).by_time_range(time_range[:first], time_range[:last]).uniq
		end

		def grouped_medications
			@_grouped_medications ||= medications.group_by &:patient_id
		end

		def process!
			grouped_medications.each do |user_id, medications|
				device_tokens = medications.first.patient.device_tokens
				
				device_tokens.each do |token|
					send_bunch medications, token.device_token
				end
			end
		end

		def send_bunch(medications, token)
			notifications = medications.inject([]) {|arr, medication| arr << build_notification(medication, token); arr }
			APNS.send_notifications notifications if notifications.any?	
		end

		def build_notification(medication, token)
			APNS::Notification.new token, alert: build_message(medication), sound: 'default', category: "DRUG_CATEGORY", other: { type: "medicine", id: medication.id, title: medication.medicine.title }
		end

		def build_message(medication)
			"Время принять #{medication.medicine.title} (#{medication.dosage} мг)!"
		end

	end
end