module Notifications
	class Messages < AlterMvc::BasicUseCase

		include ActionView::Helpers::TextHelper

		attr_reader :message
		
		def execute
			return unless send?
			process
		end

	private

		def patient
			@_patient ||= message.patient
		end

		def send?
			message.direction == 'income' && message.patient.has_devices?
		end

		def device_tokens
			@_device_tokens ||= patient.device_tokens.map &:device_token
		end

		def process
			device_tokens.each {|token| APNS.send_notification token, alert: build_text, sound: "default", category: "DOCTOR_MESSAGE_CATEGORY", other: { type: "message" } }
		end

		def build_text
			@_text ||= "Доктор: #{truncated_text}"
		end

		def truncated_text
			truncate message.text, separator: " ", length: 40
		end

	end
end