module Doctors
	class Employ < AlterMvc::BasicUseCase
		attr_reader :params

		def execute
			return doctor unless valid?
			create
			doctor
		end

		def valid?
			validator.valid?
		end

	private
		def create
			doctor.save
		end

		def doctor
			@_doctor ||= Doctor.new doctor_params
		end

		def doctor_params
			params.doctor_params
		end

		def validator
			@_validator ||= DoctorValidator.new doctor
		end
	end
end