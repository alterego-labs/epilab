module Doctors
	class UpdateProfile < AlterMvc::BasicUseCase
		attr_reader :id, :params

		def execute
			update
			doctor
		end

	private
		def update
			doctor.assign_attributes updated_attributes
			doctor.save if valid?
		end

		def valid?
			validator.valid?
		end

		def validator
			@_validator ||= DoctorValidator.new doctor
		end

		def doctor
			@_doctor ||= Doctor.find id	
		end

		def updated_attributes
			params.doctor_params
		end
	end
end