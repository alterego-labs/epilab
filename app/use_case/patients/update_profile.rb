module Patients
	class UpdateProfile < AlterMvc::BasicUseCase
		attr_reader :id, :params

		def execute
			return unless valid?
			update_patient
			patient
		end

	private

		def update_patient
			update_password? ? update_with_password : update_without_password
		end

		def update_with_password
			patient.update_with_password profile_update_params
		end

		def update_without_password
			patient.update_without_password profile_update_params
		end

		def valid?
			id && params && patient
		end

		def patient
			@_patient ||= UserQuery.find_patient_by_id id
		end

		def profile_update_params
			params.user_params
		end

		def update_password?
			profile_update_params[:password].present?
		end
	end
end