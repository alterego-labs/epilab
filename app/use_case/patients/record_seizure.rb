module Patients
	class RecordSeizure < AlterMvc::BasicUseCase
		attr_reader :patient_id, :params

		def execute
			create_seizure if valid?
			seizure
		end

	private
		def create_seizure
			seizure.save
		end

		def seizure
			@_seizure ||= patient.seizures.new seizure_params
		end

		def patient
			@_patient ||= UserQuery.find_patient_by_id patient_id
		end

		def seizure_params
			params && params.seizure_params
		end

		def validator
			@_validator ||= SeizureValidator.new seizure
		end

		def valid?
			validator.valid?
		end

	end
end