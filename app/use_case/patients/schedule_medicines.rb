module Patients
	class ScheduleMedicines < AlterMvc::BasicUseCase
		attr_reader :patient_id, :date

		def execute
			return unless patient
			patient_medicines.each {|patient_medicine| schedule! patient_medicine }
		end

	private
		def patient_medicines
			@_patient_medicines ||= patient.medicines.active
		end

		def patient
			@_patient ||= UserQuery.find_patient_by_id patient_id
		end

		def schedule!(patient_medicine)
			Scheduling::ProcessMedication.new(patient_medicine_id: patient_medicine.id, date: date).execute
		end
	end
end