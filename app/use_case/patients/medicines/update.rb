module Patients
	module Medicines
		class Update < AlterMvc::BasicUseCase
			attr_reader :medicine_id, :params

			def execute
				reception_old = medicine.reception
				medicine.assign_attributes medicine_params
				if valid?
					medicine.save
					update_todays_medications reception_old, medicine.reception
				end
				medicine
			end

			def valid?
				validator.valid?
			end

			def error_messages
				valid?
				medicine.errors.full_messages
			end

		private

			def medicine
				@_medicine ||= PatientMedicine.find_by id: medicine_id
			end

			def medicine_params
				params.patient_medicine_params
			end

			def validator
				@_validator ||= UpdatePatientMedicineValidator.new medicine
			end

			def update_todays_medications(reception_old, reception)
				marked_for_destruction = reception_old - reception
				need_to_create         = reception - reception_old
				marked_for_destruction.each do |reception|
					time_array = reception["time"].split(":").map &:to_i
					time = DateTime.now.in_time_zone.change hour: time_array[0], min: time_array[1]
					medicine.medications.where(time: time).destroy_all
				end

				Scheduling::ProcessMedication.new(patient_medicine_id: medicine.id, force: true).execute
			end

		end
	end
end