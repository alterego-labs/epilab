module Patients
	module Medicines
		class Create < AlterMvc::BasicUseCase

			attr_reader :patient_id, :params

			def execute
				create_medicine if valid?
				medicine
			end

			def valid?
				validation.valid?
			end

			def error_messages
				valid?
				medicine.errors.full_messages
			end

		private
		
			def create_medicine
				medicine.save
				schedule
			end

			def schedule
				@_schedule ||= Scheduling::ProcessMedication.new(patient_medicine_id: medicine.id).execute
			end

			def medicine
				@_medicine ||= patient.medicines.new medicine_params
			end

			def validation
				@_validator ||= PatientMedicineValidator.new medicine
			end

			def medicine_params
				params && params.patient_medicine_params
			end

			def patient
				@_patient ||= UserQuery.find_patient_by_id patient_id
			end

		end
	end
end