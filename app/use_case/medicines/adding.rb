module Medicines
	class Adding < AlterMvc::BasicUseCase
		attr_reader :params, :user_id

		def execute
			return medicine unless valid?
			create
			medicine
		end

		def valid?
			@_valid ||= validator.valid?
		end

		def error_messages
			valid?
			medicine.errors.full_messages
		end

	private

		def create
			medicine.save
		end

		def validator
			@_validator ||= MedicineValidator.new medicine
		end

		def medicine
			@_medicine ||= Medicine.new create_params
		end

		def user
			@_user ||= User.find_by id: user_id
		end

		def create_params
			medicine_params.merge patient_id: user.try(:id)
		end

		def medicine_params
			params.medicine_params rescue {}
		end
		
	end
end