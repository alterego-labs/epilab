module Api
	module Patients
		class CalendarEvents < AlterMvc::BasicUseCase
			attr_reader :patient_id
			include ApplicationHelper

			def dates
				seizure_dates & medication_dates
			end

			def images
				seizures.where("video_file_size > 0").
				group_by {|s| s.happened_at.strftime("%Y-%m-%d")}.
				inject({}) {|h, (d, s)| h[d] = add_host_prefix(s.first.video.url(:thumb)); h }
			end

		private
			def patient
				@_patient ||= UserQuery.find_patient_by_id patient_id
			end

			def seizures
				@_seizures ||= patient.seizures.order happened_at: :asc
			end

			def medications
				@_medications ||= patient.medications.order time: :asc
			end

			def seizure_dates
				seizures.map(&:date).uniq
			end

			def medication_dates
				medications.map(&:date).uniq
			end
		end
	end
end