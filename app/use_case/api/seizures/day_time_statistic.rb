module Api
	module Seizures
		class DayTimeStatistic < AlterMvc::BasicUseCase
			attr_reader :patient_id

			def execute
				grouped_seizures.inject({}) {|hash, (key, seizures)| hash.merge({key => seizures.count}) }
			end

		private
			def patient
				@_patient ||= UserQuery.find_patient_by_id patient_id
			end

			def seizures
				@_seizures ||= patient.seizures
			end

			def grouped_seizures
				seizures.group_by &:daytime
			end
		end
	end
end