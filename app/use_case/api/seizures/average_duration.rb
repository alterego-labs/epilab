module Api
	module Seizures
		class AverageDuration < AlterMvc::BasicUseCase
			attr_reader :patient_id, :interval, :count, :from

			def execute
				{ 
					items: graphic_class.new(patient_id: patient_id, items: items, label: :rejects, process_func: method(:process), calculated_period: graphic_intervals.intervals).result,
					date: (graphic_intervals.interval[:beginning_at] || "false")
				}
			end

		private
			def items
				@_seizures ||= Seizure.by_patient(patient_id)
															.by_period(fetch_beginning_at, fetch_end_at)
															.reorder happened_at: :asc
			end

			def graphic_class
				"graphics/construct/#{fetch_interval}".classify.constantize
			end

			def fetch_interval
				interval || "daily"
			end
				
			def process(date, items)
				{ data: date, average_duration: (items.sum(&:duration)/items.count rescue 0) }
			end

			def period_hash
				graphic_intervals.interval
			end

			def graphic_intervals
				@_graphic_intervals ||= Graphics::Intervals.new(user: user, count: count, beginning_at: from, interval_piece: fetch_interval)
			end

			def user
				@_user ||= User.find patient_id
			end

			def fetch_beginning_at
				period_hash[:beginning_at] ? period_hash[:beginning_at].beginning_of_day : user.created_at
			end

			def fetch_end_at
				period_hash[:end_at] ? period_hash[:beginning_at].beginning_of_day : (from ? Date.parse(from) : Date.today)
			end

		end
	end
end