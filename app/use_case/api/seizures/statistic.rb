module Api
	module Seizures
		class Statistic < AlterMvc::BasicUseCase
			attr_reader :patient_id, :period_type

			def execute
				{ items: prepare_extended_statistic }
			end

		private
			def patient
				@_patient ||= UserQuery.find_patient_by_id patient_id
			end

			def seizures
				@_seizures ||= Seizure.by_period(period[:start_at], period[:end_at]).by_patient(patient_id)
			end

			def period
				{
					start_at: (Time.zone.now - GlobalConstants::PERIOD_DISTANCE[get_period]+1.day).beginning_of_day,
					end_at: Time.zone.now.end_of_day
				}
			end

			def get_period
				period_type && period_type.to_sym || :week
			end

			def group_by_date
				@_grouped_seizures ||= seizures.group_by &:draw_date
			end

			def prepare_statistic
				@_statistic ||= group_by_date.map {|date, seizures| { data: date, seizures: seizures.length } }
			end

			def prepare_extended_statistic
				(period[:start_at].to_date..period[:end_at].to_date).inject([]) do |hash, date| 
					d = Russian::strftime date, "%d %b %y"
					hash << { data: d, seizures: get_seizures_count_for_date(d) }
					hash
				end
			end

			def get_seizures_count_for_date(d)
				group_by_date[d].try(:length) || 0
			end
		end
	end
end