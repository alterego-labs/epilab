module Api
	module Medications
		class GroupedStatistic < AlterMvc::BasicUseCase
			attr_reader :patient_id, :interval, :states, :count, :from

			def execute
				{ 
					items: graphic_class.new(patient_id: patient_id, items: items, label: :seizures, process_func: method(:process), calculated_period: graphic_intervals.intervals).result,
					series: prepare_series,
					date: (graphic_intervals.interval[:beginning_at] || "false")
				}
			end

		private
			def items
				@_items ||= Medication.by_patient(patient_id)
															.by_state(statistic_states)
															.by_period(fetch_beginning_at, fetch_end_at)
															.reorder time: :asc
			end

			def fetch_beginning_at
				period_hash[:beginning_at] ? period_hash[:beginning_at].beginning_of_day : user.created_at
			end

			def fetch_end_at
				period_hash[:end_at] ? period_hash[:beginning_at].beginning_of_day : (from ? Date.parse(from) : Date.today)
			end

			def medicines
				@_medicines ||= Rails.cache.fetch("user-#{user.id}-medicines-list-#{user.taken_medicines_count}", expires_in: 2.hours) { user.taken_medicines }
			end

			def prepare_series
				medicines.map {|medicine| { valueField: medicine.as_param, name: medicine.title } }
			end

			def graphic_class
				"graphics/construct/#{fetch_interval}".classify.constantize
			end

			def fetch_interval
				interval || "daily"
			end

			def process(date, items)
				{ data: date, medicines: process_medications(items) }
			end

			def process_medications(medications)
				medicines.inject({}) {|hash, medicine| hash[medicine.as_param] = calculate_amount(medications, medicine); hash }
			end

			def calculate_amount(medications, medicine)
				return 0 if medications.nil? || medications.empty?
				m = medications.select {|m| m.medicine_id == medicine.id }
				m && m.map(&:dosage).sum || 0
			end

			def statistic_states
				states || Patients::Constants::MEDICATION_STATES
			end

			def period_hash
				graphic_intervals.interval
			end

			def graphic_intervals
				@_graphic_intervals ||= Graphics::Intervals.new(user: user, count: count, beginning_at: from, interval_piece: fetch_interval)
			end

			def user
				@_user ||= User.find patient_id
			end

		end
	end
end