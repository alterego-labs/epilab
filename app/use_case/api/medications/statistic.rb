module Api
	module Medications
		class Statistic < AlterMvc::BasicUseCase
			attr_reader :patient_id, :states, :period_type, :type

			def execute
				{ items: items, series: prepare_series }
			end

		private
			def items
				self.send "prepare_extended_statistic#{type}"
			end

			def prepare_extended_statistic
				(period[:start_at].to_date..period[:end_at].to_date).inject([]) do |hash, date| 
					d = Russian::strftime date, "%d %b %y"
					medications = grouped_by_date[d]
					hash << { data: d, medicines: process_medications(medications) }
					hash
				end
			end

			def prepare_extended_statistic_dxchart
				(period[:start_at].to_date..period[:end_at].to_date).inject([]) do |hash, date| 
					d = Russian::strftime date, "%d %b %y"
					medications = grouped_by_date[d]
					hash << process_medications(medications).merge({data: d})
					hash
				end
			end

			def prepare_series
				medicines.map {|medicine| { valueField: medicine.as_param, name: medicine.title } }
			end

			def patient
				@_patient ||= UserQuery.find_patient_by_id patient_id
			end

			def medications
				@_medications ||= patient.medications.by_state(statistic_states).by_period(period[:start_at], period[:end_at])
			end

			def medicines
				@_medicines ||= medications.map(&:medicine).uniq
			end

			def medicine_ids
				medications.pluck(:medicine_id).uniq
			end

			def process_medications(medications)
				medicines.inject({}) {|hash, medicine| hash[medicine.as_param] = calculate_amount(medications, medicine); hash }
			end

			def calculate_amount(medications, medicine)
				return 0 if medications.nil? || medications.empty?
				m = medications.select {|m| m.medicine_id == medicine.id }
				m && m.map(&:dosage).sum || 0
			end

			def grouped_by_date
				@_grouped_by_date ||= medications.group_by &:draw_date
			end

			def period
				{
					start_at: (Time.zone.now - GlobalConstants::PERIOD_DISTANCE[get_period] + 1.day).beginning_of_day,
					end_at: Time.zone.now.end_of_day
				}
			end

			def get_period
				period_type && period_type.to_sym || :week
			end

			def statistic_states
				states || Patients::Constants::MEDICATION_STATES
			end

		end
	end
end