module DeviceTokens
	class Store < AlterMvc::BasicUseCase
		attr_reader :device_token, :user_id

		def execute
			return unless valid?
			token.set_user user.id
		end

		def valid?
			device_token && user
		end

	private
		def user
			@_user ||= User.find_by id: user_id
		end

		def token
			@_token ||= DeviceTokenQuery.fetch_by_token device_token
		end
	end
end