class UserMailer < ActionMailer::Base
  default from: "please-change-me-at-config-initializers-devise@example.com"
  #default from: "svyatoslav.bond@gmail.com"

  def welcome_email(patient)

  pdf = WickedPdf.new.pdf_from_url('http://epilab.pp.ua/year_report_statistic')
  save_path = Rails.root.join('public','report/graphs.pdf')
  File.open(save_path, 'wb') do |file|
    file << pdf
  end

    attachments['graphs.pdf'] = File.read(save_path);
    mail(to: patient.email, subject: 'Эпилаб - годовой отчет')
  end
end
