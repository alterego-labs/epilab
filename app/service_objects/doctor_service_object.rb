class DoctorServiceObject < AlterMvc::BasicServiceObject
	include ApplicationHelper
	
	def full_name
		[model.last_name, model.first_name].join(" ")
	end

  def photo_url
  	add_host_prefix(model.photo.url(:medium))
  end
end