class PatientMedicineServiceObject < AlterMvc::BasicServiceObject
	def active?
		model.finished_at == nil
	end

	def finished?
		!active?
	end

	def started?
		model.beginning_at.today? || model.beginning_at.past?
	end

	def latest_medication_date
		query.latest_medication ? query.latest_medication.time : nil
	end

	def next_medication_date
		return find_next_medication_date unless latest_medication_date
		latest_medication_date + schedule_step
	end

	def schedule_step
		GlobalConstants::PERIODICITY_DISTANCE[model.periodicity]
	end

	def should_scheduled_now?
		active? && started? && next_medication_date.today?
	end

	def should_be_scheduled?(date: Time.zone.now)
		active? && started? && next_medication_date.to_date == date.to_date
	end

	def receptions
		reception
	end

private
	def find_next_medication_date
		started? ? Time.zone.today : model.beginning_at
	end
end