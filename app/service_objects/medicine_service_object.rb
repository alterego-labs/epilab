class MedicineServiceObject < AlterMvc::BasicServiceObject
	def as_param
		model.title.parameterize
	end
end