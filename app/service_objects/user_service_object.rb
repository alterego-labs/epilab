class UserServiceObject < AlterMvc::BasicServiceObject
	include ApplicationHelper
	
	def watcher?
		patient? && model.is_watcher == true
	end

	def patient?
		model.role.to_sym == :patient
	end

	def admin?
		model.role.to_sym == :admin		
	end

	def age
		model.birthday_date && calculate_age || nil
	end

	def birthday_date
		model.birthday.present? && Date.parse(model.birthday) rescue nil
	end

  def has_role?(role_name)
    role.to_sym == role_name && !model.new_record?
  end

  def has_any_role?(roles_arr)
    roles_arr.include? role.to_sym && !model.new_record?
  end

  def should_schedule?
  	model.latest_scheduled_at ? model.latest_scheduled_at < Date.today.in_time_zone : true
  end

  def latest_scheduled_at
  	model.latest_scheduled_date && Date.parse(model.latest_scheduled_date).in_time_zone
  end

  def access_token
  	model.authentication_token
  end

  def photo_url
  	add_host_prefix(model.photo.url(:medium))
  end

  def has_devices?
  	model.device_tokens.length > 0
  end

  def daily_statistic_interval
  	# dates_array 7.days
    ((model.created_at.to_date-7.days)..Date.today).to_a
  end

  def weekly_statistic_interval
  	# dates_array(7.weeks).group_by {|d| d.strftime "%W %Y" }
    ((model.created_at.to_date.beginning_of_week-7.weeks)..Date.today).to_a.group_by {|d| d.strftime "%W %Y" }
  end

  def monthly_statistic_interval
  	# dates_array(7.months).group_by {|d| d.strftime "%m %Y" }
    ((model.created_at.to_date.beginning_of_month-7.months)..Date.today).to_a.group_by {|d| d.strftime "%m %Y" }
  end

private
	def calculate_age
		now = Time.zone.now.to_date
		now.year - model.birthday_date.year - (model.birthday_date.to_date.change(year: now.year) > now ? 1 : 0)
	end

	def dates_array(left_margin = 0.days)
		((model.created_at.to_date-left_margin)..Date.today).to_a
	end

end