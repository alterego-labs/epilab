class MedicationServiceObject < AlterMvc::BasicServiceObject
	def taken?
		model.state.to_s == "taken"
	end

	def rejected?
		model.state.to_s == "rejected"
	end

	def as_param
		model.medicine.as_param
	end

	def status
		model.state
	end

	def numeric_time
		time.hour * 3600 + time.min * 60
	end

	def date
		model.time.to_date
	end
end