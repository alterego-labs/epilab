class SeizureServiceObject < AlterMvc::BasicServiceObject
	include ApplicationHelper

	def video_url
		model.video.exists? && add_host_prefix(model.video.url) || nil
	end

	def numeric_time
		happened_at.hour * 3600 + happened_at.min * 60
	end

	def numeric_duration
		model.duration.to_i
	end

	def date
		model.happened_at.to_date
	end

	def time
		model.happened_at.to_s(:time)
	end

	def daytime
		case time.to_i
		when (0..6) then :night
		when (6..12) then :morning
		when (12..18) then :day
		else :evening
		end
	end
end