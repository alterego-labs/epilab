switchPatientTypeBlock = (role) ->
	if role == "patient"
		$(".patient-type").show()
	else
		$(".patient-type").hide()

ready = ->
	switchPatientTypeBlock $("[name='user[role]']:checked").val()

$(document).delegate "[name='user[role]']", "change", ->
	switchPatientTypeBlock @value

$(document).ready(ready)
$(document).on('page:load', ready)