$ ->
	if $("#medication-graphic").length
		$("#medication-graphic").dxChart
			dataSource: []
			series: []

			commonSeriesSettings:
				argumentField: "data"
				type: "stackedBar"
			
			legend:
				verticalAlignment: "bottom"
				horizontalAlignment: "center"
				itemTextPosition: "top"

			valueAxis:
				axisDivisionFactor: 10
				title:
					text: "миллилитров"

				position: "right"


			title: "Прием препаратов"
			tooltip:
				enabled: true
				font:
					family: "Lato"
					size: "13px"
				customizeText: ->
					"<b>" + @seriesName + ":</b> " + @valueText + "мг"

		loadMedicationsDataSource "week"

		$("[name=period]").on "change", ->
			loadMedicationsDataSource $(@).val() if medication_period isnt $(@).val()