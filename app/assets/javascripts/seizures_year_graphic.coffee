$ ->
	if $("#seizures-graphic").length
		$("#seizures-graphic").dxChart
			dataSource: []

			commonSeriesSettings:
				argumentField: "data"
				type: "stackedLine"

			series: [
				valueField: "seizures"
				name: "Приступы"
			]
			
			legend:
	      visible: false

			valueAxis:		
				tickInterval: 1
				title:
					text: "приступов"

				position: "right"

			title: "Приступы"
			tooltip:
				enabled: true
				font:
					family: "Lato"
					size: "13px"

		loadSeizuresDataSource "year"

		$("[name=period]").on "change", ->
			loadSeizuresDataSource $(@).val() if seizure_period isnt $(@).val()