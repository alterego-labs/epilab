ready = ->
	patients = new Bloodhound(
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace("full_name")
		queryTokenizer: Bloodhound.tokenizers.whitespace
		remote: '/admin/patients/search?query=%QUERY'
	)

	patients.initialize()

	$("#patient-search").typeahead
		hint: true
		highlight: true
		minLength: 1
	,
		name: "patients"
		displayKey: "full_name"
		source: patients.ttAdapter()
		templates:
				empty: [
					"<div class=\"empty-message\">"
					"ни одного пациента по Вашему запросу не было найдено"
					"</div>"
				].join("\n")
				suggestion: Handlebars.compile("<p class='typeahead-search'><div class='typeahead-photo'><img src='{{photo}}'></div><div class='typeahead-info'>{{full_name}}<small>{{email}}</small></div></p>")


$(document).delegate "#patient-search", "typeahead:selected", (obj, data, name) ->
	window.location.href = data.url

$(document).ready(ready)
$(document).on('page:load', ready)