# Focus state for append/prepend inputs
$(document).delegate(".form-control", "focus", ->
	$(this).closest(".input-group, .form-group").addClass "focus"
	return
).on "blur", ".form-control", ->
	$(this).closest(".input-group, .form-group").removeClass "focus"
	return

autoCloseAlerts = ->
	window.setTimeout (->
		$(".dialog.dialog-success").fadeTo(500, 0).slideUp 500, ->
			return $(this).remove()
	), 5000


ready = ->
	autoCloseAlerts()

	# jQuery UI Datepicker
	datepickerSelector = ".flat-datepicker"
	$(datepickerSelector).datepicker(
		showOtherMonths: true
		selectOtherMonths: true
		dateFormat: "dd.mm.yy"
		yearRange: "-1:+1"
	)
	$(datepickerSelector).prev(".btn").on "click", (e) ->
		$(datepickerSelector).focus()
		return

	$.extend $.datepicker,
		_checkOffset: (inst, offset, isFixed) ->
			offset


	# Now let's align datepicker with the prepend button
	$(datepickerSelector).datepicker("widget").css "margin-left": -$(datepickerSelector).prev(".input-group-btn").find(".btn").outerWidth()
	
	$(".tagsinput").tagsInput()

	$(".table-sortable td").each ->
		$(this).css "width", $(this).innerWidth() 

	$('.table-sortable tbody').sortable
		handle: 'i.fui-list',
		helper: 'clone',
		update: (event, list) ->
			object =
				id: list.item[0].id
				position: list.item[0].rowIndex
			$.post $(this).data("update-url"),
				object: object

	$('.table-sortable tbody').disableSelection()

	$('.nav-tabbable a').on 'click', (e) ->
		e.preventDefault()
		$(@).tab "show"
		title = $("title").text()
		url = window.location.pathname + $(this).attr("href").replace(/#/g, '?tab=')
		history.replaceState title, title, url
		$(document).trigger "resize"

	$('textarea').autosize()

	$('input[data-mask]').each ->
		$(@).inputmask mask: $(@).data("mask")
	$('input[data-type-mask]').each ->
		$(@).inputmask $(@).data("type-mask")

$(document).ready(ready)
$(document).on('page:load', ready)