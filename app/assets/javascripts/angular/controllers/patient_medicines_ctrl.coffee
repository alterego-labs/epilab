# window.PatientMedicinesCtrl = ($scope, $http, $filter) ->
window.PatientMedicinesCtrl = [
	"$scope"
	"$http"
	"$filter"
	"$modal"
	(scope, http, filter, modal) ->
		scope.medicines = []
		scope.medicine  = null
		scope.receptions = [{time: "00:00"}]

		http.get("/api/v1/medicines.json").success((response) ->
			scope.medicines = response.medicines
			scope.medicine  = scope.medicines[0].id
		).error ->
			alert "Sorry, but something went wrong.."

		scope.custom_medicine = ->
			modalInstance = modal.open(
				templateUrl: "custom_medicine.html"
				controller: CustomMedicineInstanceCtrl
				resolve:
					items: ->
						scope.medicines
			)
			modalInstance.result.then ((selectedItem) ->
				scope.medicines.push selectedItem
				scope.medicine = selectedItem.id
				return
			), (e)->
				return

		scope.add_reception = ->
			scope.receptions.push {time: "00:00"}
			return

		scope.is_acceptable = (reception) ->
			reception.dosage and reception.dosage isnt "" and reception.time isnt ""
]

window.CustomMedicineInstanceCtrl = [
	"$scope"
	"$modalInstance"
	"$http"
	"items"
	(scope, modalInstance, http, items) ->
		scope.medicine = {}

		scope.ok = ->
			http.post("/api/v1/medicines.json",
				medicine: scope.medicine
			).success( (data, status) ->
				modalInstance.close data.medicine
			).error (data, status) ->
				alert data.errors
		  
		  return

		scope.cancel = ->
		  modalInstance.dismiss "cancel"
		  return

]