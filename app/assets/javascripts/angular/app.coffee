angular.module("epilab", [
	"ngResource",
	"ngRoute",
	"ngSanitize",
	"ui.bootstrap",
	"ng-rails-csrf"
])

$(document).on 'ready page:load', ->
  angular.bootstrap(document.body, ['epilab'])