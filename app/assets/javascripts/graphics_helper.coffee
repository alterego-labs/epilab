window.loadMedicationsDataSource = (period) ->
	window.medication_period = period
	$.getJSON "/api/v1/patients/#{patient_id}/medications/graphic?period=#{period}&dx_chart=true", (data) ->
		chart = $("#medication-graphic").dxChart "instance"
		chart.option 'dataSource', data.items if data.items.length > 1
		chart.option 'series', data.series if data.series.length > 1
		return

window.loadSeizuresDataSource = (period) ->
	window.seizure_period = period
	$.getJSON "/api/v1/patients/#{patient_id}/seizures/graphic?period=#{period}&dx_chart=true", (data) ->
		chart = $("#seizures-graphic").dxChart "instance"
		chart.option 'dataSource', data.items if data.items.length >= 1
		return