initialize = ->
	mapCanvas = document.getElementById "map-canvas"
	mapOptions =
		center: new google.maps.LatLng(44.5403, -78.5463)
		zoom: 8
		mapTypeId: google.maps.MapTypeId.ROADMAP
		zoomControl: false
		panControl: false
		mapTypeControl: false
		scaleControl: false
		streetViewControl: false
		overviewMapControl: false
	window.map = new google.maps.Map mapCanvas, mapOptions

	google.maps.event.addListenerOnce window.map, 'bounds_changed', (event) ->
	  this.setZoom 16 if this.getZoom() > 16
  lng = document.getElementById("address").dataset.longitude
  ltd = document.getElementById("address").dataset.latitude
	window.addMarker lng, ltd

window.addMarker = (lng, ltd) ->
	bounds = new google.maps.LatLngBounds()
	position = new google.maps.LatLng lng, ltd
	bounds.extend position
	marker = new google.maps.Marker(
		position: position
		map: window.map
		title: " "
		animation: google.maps.Animation.DROP
	)
	window.map.fitBounds bounds

google.maps.event.addDomListener(window, 'load', initialize);