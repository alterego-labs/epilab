# CONSTANTS
PATIENTS_COUNT = 20
MEDICINES 		 = %w(Антелепсин Бензонал Фенитоин Вигабатрин Паглюферал Малиазин Фенобарбитал Карбапин Депакин Конвулекс)
DOSAGES   	   = %w(10мл 20мл 30мл 40мл 50мл 60мл 70мл 80мл 90мл)
TIMES 				 = %w(08:00 08:30 10:00 11:30 12:00 12:30 14:00 14:30 16:00 16:30 17:00 19:00 19:30 21:00)
STATISTIC_DAYS = 200
DATE 					 = Time.zone.now

p "Clear Database"
Medication.destroy_all
PatientMedicine.destroy_all
Medicine.destroy_all
UserQuery.patients.destroy_all
Seizure.destroy_all

p "reset_pk_sequence"
ActiveRecord::Base.connection.reset_pk_sequence!('medications')
ActiveRecord::Base.connection.reset_pk_sequence!('patient_medicines')
ActiveRecord::Base.connection.reset_pk_sequence!('medicines')
ActiveRecord::Base.connection.reset_pk_sequence!('users')
ActiveRecord::Base.connection.reset_pk_sequence!('seizures')

p "Create Medicines"
MEDICINES.each do |medicine| 
	Medicine.create title: medicine
end

p "Create Patients"
PATIENTS_COUNT.times.each do
	User.create first_name: Faker::Name.first_name, 
							last_name: Faker::Name.last_name,
							email: Faker::Internet.email, 
							password: :password, 
							birthday: (20..60).to_a.sample.years.ago, 
							sex: "male", 
							role: "patient"
end

p "Create Patient's Medicine"
patient   = UserQuery.patients.first
patient.update email: "patient@epilab.ru"
p "#{patient.full_name}, #{patient.email}"
medicines = Medicine.pluck(:id).sample((3..6).to_a.sample)
time 			= TIMES.sample(medicines.length).sort

medicines.each_with_index do |medicine_id, index|
	medicine = Medicine.find medicine_id
	patient.medicines.create medicine_id: medicine_id,
													 time: time[index],
													 dosage: 10
end

p "Create Medications"
(0..STATISTIC_DAYS).to_a.each do |subtrahend|
	schedule_date = DATE - subtrahend.days
	Patients::ScheduleMedicines.new(patient_id: patient.id, date: schedule_date).execute
end

p "Take/Reject Medications"
rejected_count 					= (Medication.count*0.2).to_i
rejected_medication_ids = Medication.pluck(:id).sample(rejected_count)
Medication.find_each {|medication| medication.taken! }
Medication.where(id: rejected_medication_ids).find_each {|medication| medication.rejected! }

p "Create Seizures"
(0..STATISTIC_DAYS).to_a.each do |subtrahend|
	seizure_times = TIMES.sample((1..5).to_a.sample)
	seizure_times.each do |time|
		hour = time.split(":")[0].to_i
		min  = time.split(":")[1].to_i
		patient.seizures.create type: I18n.t("dictionary.seizure_types").sample,
														reason: I18n.t("dictionary.seizure_reasons").sample,
														duration: (5..20).to_a.sample,
														happened_at: (DATE-subtrahend.days).change({ hour: hour, min: min })
	end
end