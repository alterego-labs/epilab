# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150507093347) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "clinics", force: true do |t|
    t.text     "about"
    t.string   "coordinates"
    t.string   "address"
    t.string   "phone"
    t.string   "email"
    t.string   "site"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "consultations", force: true do |t|
    t.integer  "patient_id"
    t.integer  "author_id"
    t.integer  "direction",  default: 0
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "dialog_id"
    t.boolean  "is_read",    default: false
  end

  add_index "consultations", ["author_id"], name: "index_consultations_on_author_id", using: :btree
  add_index "consultations", ["dialog_id"], name: "index_consultations_on_dialog_id", using: :btree
  add_index "consultations", ["patient_id"], name: "index_consultations_on_patient_id", using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "device_tokens", force: true do |t|
    t.integer  "user_id"
    t.string   "device_token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "device_tokens", ["user_id"], name: "index_device_tokens_on_user_id", using: :btree

  create_table "devise_tokens", force: true do |t|
    t.integer  "user_id"
    t.string   "devise_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "devise_tokens", ["user_id"], name: "index_devise_tokens_on_user_id", using: :btree

  create_table "dialogs", force: true do |t|
    t.integer  "patient_id"
    t.integer  "messages_count"
    t.integer  "unread_by_patient", default: 0
    t.integer  "unread_by_doctor",  default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dialogs", ["patient_id"], name: "index_dialogs_on_patient_id", using: :btree

  create_table "dictionary_items", force: true do |t|
    t.string   "type"
    t.string   "title"
    t.integer  "position",   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "doctors", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.text     "about"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position"
    t.string   "job"
  end

  create_table "medications", force: true do |t|
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state",               default: "scheduled", null: false
    t.integer  "medicine_id"
    t.integer  "dosage"
    t.integer  "patient_id"
    t.integer  "patient_medicine_id"
  end

  add_index "medications", ["medicine_id"], name: "index_medications_on_medicine_id", using: :btree
  add_index "medications", ["patient_id"], name: "index_medications_on_patient_id", using: :btree
  add_index "medications", ["patient_medicine_id"], name: "index_medications_on_patient_medicine_id", using: :btree
  add_index "medications", ["time"], name: "index_medications_on_time", using: :btree

  create_table "medicines", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position",   default: 0, null: false
    t.integer  "patient_id"
  end

  add_index "medicines", ["patient_id"], name: "index_medicines_on_patient_id", using: :btree
  add_index "medicines", ["position"], name: "index_medicines_on_position", using: :btree

  create_table "patient_medicines", force: true do |t|
    t.integer  "user_id"
    t.integer  "medicine_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "beginning_at"
    t.integer  "periodicity"
    t.text     "reception"
    t.boolean  "remind",       default: true, null: false
    t.datetime "finished_at"
  end

  add_index "patient_medicines", ["medicine_id"], name: "index_patient_medicines_on_medicine_id", using: :btree
  add_index "patient_medicines", ["user_id"], name: "index_patient_medicines_on_user_id", using: :btree

  create_table "seizures", force: true do |t|
    t.integer  "patient_id"
    t.string   "type"
    t.string   "reason"
    t.integer  "duration"
    t.text     "comment"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "happened_at"
  end

  add_index "seizures", ["happened_at"], name: "index_seizures_on_happened_at", using: :btree

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "role"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.hstore   "details"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "seizures_count",         default: 0,  null: false
    t.string   "authentication_token"
    t.integer  "sign_in_count"
    t.integer  "medicines_count",        default: 0,  null: false
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
