class RemoveMedicationIdFromMedication < ActiveRecord::Migration
  def change
  	remove_column :medications, :medication_id
  end
end
