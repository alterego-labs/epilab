class AddPatientMedicineIdToMedications < ActiveRecord::Migration
  def change
    add_column :medications, :patient_medicine_id, :integer
    add_index :medications, :patient_medicine_id
  end
end
