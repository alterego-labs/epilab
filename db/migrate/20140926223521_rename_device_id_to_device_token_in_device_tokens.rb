class RenameDeviceIdToDeviceTokenInDeviceTokens < ActiveRecord::Migration
  def change
  	rename_column :device_tokens, :devise_id, :device_token
  end
end
