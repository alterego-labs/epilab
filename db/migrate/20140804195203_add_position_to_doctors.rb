class AddPositionToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :position, :integer
  end
end
