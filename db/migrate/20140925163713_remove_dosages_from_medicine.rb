class RemoveDosagesFromMedicine < ActiveRecord::Migration
  def change
  	remove_column :medicines, :dosages
  end
end
