class CreateAttacks < ActiveRecord::Migration
  def change
    create_table :attacks do |t|
      t.integer :patient_id, index: true
      t.string :type
      t.string :reason
      t.string :duration
      t.string :date, index: true
      t.text :comment
      t.attachment :video

      t.timestamps
    end
  end
end
