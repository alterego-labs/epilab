class FixBeginningFieldNameInPatientMedicines < ActiveRecord::Migration
  def change
  	rename_column :patient_medicines, :beggining_at, :beginning_at
  end
end
