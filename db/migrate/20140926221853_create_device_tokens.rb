class CreateDeviceTokens < ActiveRecord::Migration
  def change
    create_table :device_tokens do |t|
      t.references :user, index: true
      t.string :devise_id

      t.timestamps
    end
  end
end
