class AddStateToMedicationSchedules < ActiveRecord::Migration
  def change
    add_column :medication_schedules, :state, :string
    remove_column :medication_schedules, :taken
  end
end
