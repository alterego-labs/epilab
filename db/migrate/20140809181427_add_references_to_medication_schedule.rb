class AddReferencesToMedicationSchedule < ActiveRecord::Migration
  def change
    add_column :medication_schedules, :patient_id, :string
    add_column :medication_schedules, :medicine_id, :integer
    add_column :medication_schedules, :dosage, :integer
  end
end
