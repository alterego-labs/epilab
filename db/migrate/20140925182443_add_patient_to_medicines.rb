class AddPatientToMedicines < ActiveRecord::Migration
  def change
    add_column :medicines, :patient_id, :integer
    add_index :medicines, :patient_id
  end
end
