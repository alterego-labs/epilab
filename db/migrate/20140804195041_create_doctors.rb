class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :first_name
      t.string :last_name
      t.text :about
      t.attachment :photo

      t.timestamps
    end
  end
end
