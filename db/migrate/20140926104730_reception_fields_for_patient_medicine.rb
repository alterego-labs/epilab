class ReceptionFieldsForPatientMedicine < ActiveRecord::Migration
  def change
  	add_column :patient_medicines, :beggining_at, :date
  	add_column :patient_medicines, :periodicity, :integer
  	add_column :patient_medicines, :reception, :text
  	remove_column :patient_medicines, :dosage
  	remove_column :patient_medicines, :time
  end
end
