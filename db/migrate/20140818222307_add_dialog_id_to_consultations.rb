class AddDialogIdToConsultations < ActiveRecord::Migration
  def change
    add_column :consultations, :dialog_id, :integer
    add_index :consultations, :dialog_id
  end
end
