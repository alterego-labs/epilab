class AddDetailsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :details, :hstore
  end
end
