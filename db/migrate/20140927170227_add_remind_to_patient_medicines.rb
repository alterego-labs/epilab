class AddRemindToPatientMedicines < ActiveRecord::Migration
  def change
    add_column :patient_medicines, :remind, :boolean, null: false, default: false
  end
end
