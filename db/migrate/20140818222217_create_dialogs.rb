class CreateDialogs < ActiveRecord::Migration
  def change
    create_table :dialogs do |t|
      t.references :patient, index: true
      t.integer :messages_count
      t.integer :unread_by_patient, default: 0
      t.integer :unread_by_doctor, default: 0

      t.timestamps
    end
  end
end
