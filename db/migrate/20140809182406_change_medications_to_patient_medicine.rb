class ChangeMedicationsToPatientMedicine < ActiveRecord::Migration
  def change
  	rename_table :medications, :patient_medicines
  	rename_table :medication_schedules, :medications
  end
end
