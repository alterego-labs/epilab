class AddPositionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :position, :integer, null: false, default: 0
    add_index :users, :position
    remove_column :users, :sex
  end
end
