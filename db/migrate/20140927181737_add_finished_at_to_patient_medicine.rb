class AddFinishedAtToPatientMedicine < ActiveRecord::Migration
  def change
    add_column :patient_medicines, :finished_at, :datetime
  end
end
