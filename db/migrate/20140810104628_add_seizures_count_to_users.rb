class AddSeizuresCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :seizures_count, :integer, null: false, default: 0

    User.reset_column_information
    User.all.each { |u| u.update seizures_count: u.seizures.length }
  end
end
