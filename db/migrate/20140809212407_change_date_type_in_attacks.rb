class ChangeDateTypeInAttacks < ActiveRecord::Migration
  def change
  	remove_column :attacks, :date
  	add_column :attacks, :happened_at, :datetime
  end
end
