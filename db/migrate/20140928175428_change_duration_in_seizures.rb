class ChangeDurationInSeizures < ActiveRecord::Migration
  def change
  	change_column :seizures, :duration, 'integer USING CAST(duration AS integer)' 
  end
end