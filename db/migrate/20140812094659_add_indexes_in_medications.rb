class AddIndexesInMedications < ActiveRecord::Migration
  def change
  	add_index :medications, :patient_id
  	add_index :medications, :medicine_id
  end
end
