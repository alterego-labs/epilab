class CreateMedicationSchedules < ActiveRecord::Migration
  def change
    create_table :medication_schedules do |t|
      t.references :medication, index: true
      t.string :taken
      t.datetime :time

      t.timestamps
    end
  end
end
