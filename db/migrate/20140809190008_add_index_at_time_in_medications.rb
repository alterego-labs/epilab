class AddIndexAtTimeInMedications < ActiveRecord::Migration
  def change
  	add_index :medications, :time
  end
end
