class ChangeStateInMedicationSchedules < ActiveRecord::Migration
  def change
  	change_column :medication_schedules, :state, :string, null: false, default: "scheduled"
  end
end
