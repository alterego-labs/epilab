class RemoveBirthdayFromUsers < ActiveRecord::Migration
  def change
  	remove_column :users, :birthday
  	remove_column :users, :position
  end
end
