class AddMedicinesCountToUser < ActiveRecord::Migration
  def change
    add_column :users, :medicines_count, :integer, null: false, default: 0

    User.reset_column_information
    User.all.each { |u| u.update medicines_count: u.medicines.length }
  end
end
