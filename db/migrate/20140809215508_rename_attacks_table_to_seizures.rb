class RenameAttacksTableToSeizures < ActiveRecord::Migration
  def change
  	rename_table :attacks, :seizures
  end
end
