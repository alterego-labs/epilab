class CreateMedications < ActiveRecord::Migration
  def change
    create_table :medications do |t|
      t.references :user, index: true
      t.references :medicine, index: true
      t.string :time
      t.integer :dosage

      t.timestamps
    end
  end
end
