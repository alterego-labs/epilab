class ChangeColumnPatientIdInMedications < ActiveRecord::Migration
  def change
  	remove_index :medications, :patient_id
  	remove_column :medications, :patient_id
  	
  	add_column :medications, :patient_id, :integer
  	add_index :medications, :patient_id
  end
end
