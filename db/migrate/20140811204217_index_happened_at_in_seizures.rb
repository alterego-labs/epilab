class IndexHappenedAtInSeizures < ActiveRecord::Migration
  def change
  	add_index :seizures, :happened_at
  end
end
