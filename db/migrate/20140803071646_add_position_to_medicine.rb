class AddPositionToMedicine < ActiveRecord::Migration
  def change
    add_column :medicines, :position, :integer, null: false, default: 0
    add_index :medicines, :position
  end
end
