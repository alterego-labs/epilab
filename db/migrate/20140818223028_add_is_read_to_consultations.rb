class AddIsReadToConsultations < ActiveRecord::Migration
  def change
    add_column :consultations, :is_read, :boolean, default: false
  end
end
