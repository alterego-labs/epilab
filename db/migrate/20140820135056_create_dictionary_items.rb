class CreateDictionaryItems < ActiveRecord::Migration
  def change
    create_table :dictionary_items do |t|
      t.string :type, index: true
      t.string :title
      t.integer :position, default: 0

      t.timestamps
    end
  end
end
