class CreateConsultations < ActiveRecord::Migration
  def change
    create_table :consultations do |t|
      t.references :patient, index: true
      t.references :author, index: true
      t.integer :direction, index: true, default: 0
      t.text :text

      t.timestamps
    end
  end
end
