class ChangeDefaultForRemindInPatientMedicines < ActiveRecord::Migration
  def change
  	change_column :patient_medicines, :remind, :boolean, default: true, null: false
  end
end
