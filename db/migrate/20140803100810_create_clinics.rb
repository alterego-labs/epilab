class CreateClinics < ActiveRecord::Migration
  def change
    create_table :clinics do |t|
      t.text :about
      t.string :coordinates
      t.string :address
      t.string :phone
      t.string :email
      t.string :site

      t.timestamps
    end
  end
end
