describe "Medicine" do

	let(:user) { create(:patient) }

	before :all do
		create_list(:medicine, 5)
	end

	context "failed" do
		specify "without access_token" do
			get "/api/v1/medicines"
		end

		specify "with invalid access_token" do
			get "/api/v1/medicines", { access_token: "invalid_token" }
		end

		after :each do
			expect(status).to be 401
			expect(json).to have_key "error"
		end
	end

	context "success" do
		it "should return all medicines" do
			get "api/v1/medicines", { access_token: user.access_token }
			expect(status).to be 200
			expect(json).to have_key :medicines
			expect(json[:medicines]).to eq medicines_json.map(&:with_indifferent_access)
		end
	end

end