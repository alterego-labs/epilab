describe "clinic" do
	let(:admin) { create(:admin) }
	let(:patient) { create(:patient) }

	before { create(:clinic) }

	describe "#show" do
		let(:url) { "api/v1/clinic.json" }

		context "failed" do
			specify "without access_token" do
				get url
			end

			specify "with invalid access_token" do
				get url, { access_token: "invalid_access_token" }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key :error
			end
		end

		context "success" do
			specify "with patient access_token" do
				get url, { access_token: patient.access_token }
			end

			specify "with admin access_token" do
				get url, { access_token: admin.access_token }
			end

			after :each do
				expect(status).to be 200
				expect(json).to include :page
			end
		end
	end

end