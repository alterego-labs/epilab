describe "doctors" do
	let(:admin) { create(:admin) }
	let(:patient) { create(:patient) }

	before :each do
		Array.new(10) { create(:doctor) }
	end

	describe "#index" do
		let(:url) { "api/v1/doctors.json" }
		context "failure" do
			specify "without access_token" do
				get url
			end

			specify "with invalid access_token" do
				get url, { access_token: "invalid_access_token" }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key :error
			end
		end

		context "success" do
			specify "with patient access_token" do
				get url, { access_token: patient.access_token }
			end

			specify "with admin access_token" do
				get url, { access_token: admin.access_token }
			end

			after :each do
				expect(status).to be 200
				expect(json).to have_key :doctors
				expect(json[:doctors].length).to be 10
				expect(json[:doctors].first).to include :id, :full_name, :photo, :about
			end
		end
	end

end