describe "Seizure" do
	let(:admin) { create(:admin) }
	let(:patient) { create(:patient) }
	let(:another_patient) { create(:patient) }

	describe "#today" do

		context "failure" do
			specify "without access_token" do
				get "api/v1/patients/#{patient.id}/seizures/today"
				expect(status).to be 401
			end

			specify "with invalid access_token" do
				get "api/v1/patients/#{patient.id}/seizures/today", { access_token: "invalid_token" }
				expect(status).to be 401
			end

			specify "pacient not found" do
				get "api/v1/patients/999/seizures/today"
				expect(status).to be 404
			end

			specify "with another patient access_token" do
				get "api/v1/patients/#{patient.id}/seizures/today", { access_token: another_patient.access_token }
				expect(status).to be 401
			end

			after { expect(json).to have_key "error" }
		end

		context "success" do
			specify "patient with correct access_token" do
				get "api/v1/patients/#{patient.id}/seizures/today", { access_token: patient.access_token }
			end

			specify "admin with correct access_token" do
				get "api/v1/patients/#{patient.id}/seizures/today", { access_token: admin.access_token }
			end

			after :each do
				expect(status).to be 200
				expect(json).to have_key :seizures
				expect(json[:seizures]).to eq seizures_json_for(patient).map(&:with_indifferent_access)
			end
		end


	end

	context "#graphic" do

		context "failure" do
			specify "without access_token" do
				get "api/v1/patients/#{patient.id}/seizures/graphic"
				expect(status).to be 401
			end

			specify "with invalid access_token" do
				get "api/v1/patients/#{patient.id}/seizures/graphic", { access_token: "invalid_token" }
				expect(status).to be 401
			end

			specify "pacient not found" do
				get "api/v1/patients/999/seizures/graphic"
				expect(status).to be 404
			end

			specify "with another patient access_token" do
				get "api/v1/patients/#{patient.id}/seizures/graphic", { access_token: another_patient.access_token }
				expect(status).to be 401
			end

			after { expect(json).to have_key "error" }
		end

		context "success" do
			specify "patient with correct access_token" do
				get "api/v1/patients/#{patient.id}/seizures/graphic", { access_token: patient.access_token }
			end

			specify "admin with correct access_token" do
				get "api/v1/patients/#{patient.id}/seizures/graphic", { access_token: admin.access_token }
			end

			after :each do
				expect(status).to be 200
				expect(json).to have_key :items
				expect(json[:items].length).to be 7
			end
		end

	end

	describe "#create" do
		let(:url) { "api/v1/patients/#{patient.id}/seizures" }

		context "unauthorize" do
			specify "without access_token" do
				post url
				expect(status).to be 401
			end

			specify "with invalid access_token" do
				post url, { access_token: "invalid_token" }
				expect(status).to be 401
			end

			specify "pacient not found" do
				post "api/v1/patients/999/seizures"
				expect(status).to be 404
			end

			specify "with another patient access_token" do
				post url, { access_token: another_patient.access_token }
				expect(status).to be 401
			end

			after { expect(json).to have_key "error" }
		end

		context "failure" do
			specify "without params" do
				post url, { access_token: patient.access_token }
				expect(json[:errors]).to include :type, :duration, :happened_at, :reason
			end

			specify "with string duration" do
				post url, { access_token: patient.access_token, seizure: invalid_seizure_duration_params }
				expect(json[:errors]).to have_key :duration
			end

			after :each do
				expect(status).to be 422
				expect(json).to have_key "errors"
			end
		end

		context "success" do
			specify "with correct access_token and params" do
				post url, { access_token: patient.access_token, seizure: valid_seizure_params }
			end

			after :each do
				expect(json).to have_key :seizure
				expect(json[:seizure]).to eq seizure_json_for(Seizure.find(11)).with_indifferent_access
				expect(status).to be 200
			end
		end

	end

	describe "#show" do
		let(:seizure) { create(:seizure, user: patient) }
		let(:url) { "api/v1/patients/#{patient.id}/seizures/#{seizure.id}" }

		context "unauthorize" do
			specify "without access_token" do
				get url
				expect(status).to be 401
			end

			specify "with invalid access_token" do
				get url, { access_token: "invalid_token" }
				expect(status).to be 401
			end

			specify "pacient not found" do
				get "api/v1/patients/999/seizures/#{seizure.id}"
				expect(status).to be 404
			end

			specify "with another patient access_token" do
				get url, { access_token: another_patient.access_token }
				expect(status).to be 401
			end

			after { expect(json).to have_key "error" }
		end

		context "success" do
			specify "with correct access_token and params" do
				get url, { access_token: patient.access_token }
			end

			after :each do
				expect(json['seizure']).to eq seizure_json_for(seizure).with_indifferent_access
				expect(status).to be 200
			end
		end

	end

end