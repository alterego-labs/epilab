describe "consultations" do
	
	let(:admin) { create :admin }
	let(:patient) { create :patient }
	let(:another_patient) { create :patient }
	let(:dialog) { create :dialog, patient_id: patient.id }

	before :each do
		Array.new(2) { create :consultation, patient_id: patient.id, author_id: patient.id, dialog_id: dialog.id }
		Array.new(1) { create :consultation, patient_id: patient.id, author_id: admin.id,   dialog_id: dialog.id }
	end

	describe "#index" do
		let(:url) { "api/v1/patients/#{patient.id}/consultations" }
		context "unauthorized" do
			specify "without access_token" do
				get url
			end

			specify "with invalid access_token" do
				get	url, { access_token: "invalid_access_token" }
			end

			specify "with other patient access_token" do
				get url, { access_token: another_patient.access_token }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key "error"
			end
		end

		context "failed" do
			specify "patient not found" do
				get "api/v1/patients/999/consultations", { access_token: patient.access_token }
				expect(status).to be 404
				expect(json).to have_key "error"
			end
		end

		context "success" do
			specify "with patient access_token" do
				get url, { access_token: patient.access_token }
				expect(status).to be 200
				expect(json).to have_key :consultations
				expect(json["consultations"].length).to be 3
				expect(json["consultations"].first).to include :id, :time, :text, :direction, :author
			end
		end
	end

	describe "#create" do
		let(:url) { "api/v1/patients/#{patient.id}/consultations" }
		context "unauthorized" do
			specify "without access_token" do
				post url
			end

			specify "with invalid access_token" do
				post	url, { access_token: "invalid_access_token" }
			end

			specify "with other patient access_token" do
				post url, { access_token: another_patient.access_token }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key "error"
			end
		end

		context "failed" do
			specify "patient not found" do
				post "api/v1/patients/999/consultations", { access_token: patient.access_token }
				expect(status).to be 404
				expect(json).to have_key "error"
			end

			specify "invalid params" do
				post url, { access_token: patient.access_token }
				expect(status).to be 422
				expect(json).to have_key "errors"
			end
		end

		context "success" do
			specify "with patient access_token" do
				post url, { access_token: patient.access_token, consultation: { text: "Lorem ipsum.." } }

				expect(status).to be 200
				expect(json).to have_key :consultation
				expect(json["consultation"]).to include :id, :time, :text, :direction, :author
			end
		end
	end

end