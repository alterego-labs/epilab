describe "authorization" do

	context "failed" do

		context "signin" do
			specify "without any params" do
				post "/api/v1/user/login"
			end

			specify "with invalid params" do
				post "api/v1/user/login", { user: { email: "admin@example.com", password: :password } }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key 'error'
			end
		end

		context "logout" do
			specify "without access_token" do
				post "api/v1/user/logout"
			end

			specify "with invalid access_token" do
				post "api/v1/user/logout", { access_token: "invalid_token" }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key 'error'
			end
		end

		context "signup" do
			specify "without any params" do
				post "api/v1/user/signup"
			end

			specify "with invalid params" do
				post "api/v1/user/signup", { user: { email: "admin@example.com", password: :password } }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key 'errors'
			end
		end

		context "update profile auth error" do
			specify "with invalid access_token" do
				post "api/v1/user/update", { user: valid_user_update_params, access_token: "invalid_token_there" }
			end

			specify "without access_token" do
				post "api/v1/user/update", { user: valid_user_update_params }
			end

			specify "without any params" do
				post "api/v1/user/update"
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key 'error'
			end
		end

		context "update profile validation errors" do
			let(:user) { create(:patient) }

			specify "with invalid params" do
				post "api/v1/user/update", { user: invalid_user_update_params, access_token: user.access_token }
			end

			specify "with invalid password_confirmation" do
				post "api/v1/user/update", { user: invalid_user_password_confirmation_params, access_token: user.access_token }
				expect(json[:errors]).to have_key 'password_confirmation'
			end

			specify "with invalud current_password" do
				post "api/v1/user/update", { user: invalid_user_current_password_params, access_token: user.access_token }

				expect(json[:errors]).to have_key 'current_password'
			end

			after :each do
				expect(status).to be 422
				expect(json).to have_key 'errors'
			end
		end

	end

	context "success" do
		let(:user) { create :patient }
		let(:new_user) { build :patient }

		context "signin" do
			before :each do
				post "api/v1/user/login", { user: { email: user.email, password: :password } }
			end

			it "should be ok" do
				expect(json).to include :access_token, :user
			end

			it "should return actual data" do
				expect(json).to eq correct_auth_data
			end
		end

		context "logout" do
			it "should logout user" do
				post "api/v1/user/logout", { access_token: user.access_token }
				expect(status).to be 200
			end
		end

		context "signup" do
			before :each do
				post "api/v1/user/signup", { user: new_user.as_json.merge({password: :password }) }
			end

			it "should be ok" do
				expect(json).to include :access_token, :user
			end

			it "should return actual data" do
				expect(json).to eq correct_auth_data
			end
		end

		context "update" do
			specify "any user attributes" do
				post "api/v1/user/update", { user: valid_user_update_params, access_token: user.access_token }
			end

			specify "password" do
				post "api/v1/user/update", { user: valid_user_update_password_params, access_token: user.access_token }
			end
		end

		after(:each) do
			expect(status).to be 200
		end

	end

end