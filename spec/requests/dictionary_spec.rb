describe "dictionary" do
	let(:admin) { create(:admin) }
	let(:patient) { create(:patient) }
	
	before :each do
		Array.new(2) { |i| create(:seizure_type, title: "Seizure Type #{i}", position: 2-i)  }
		Array.new(3) { |i| create(:seizure_reason, title: "Seizure Reason #{i}")  }
	end	

	describe "get seizure_types" do
		let(:url) { "/api/v1/dictionary/seizure/types" }

		context "failure" do
			specify "without access_token" do
				get url
			end

			specify "with invalid access_token" do
				get url, { access_token: "invalid_access_token" }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key :error
			end
		end

		context "success" do
			specify "with patient access_token" do
				get url, { access_token: patient.access_token }
			end

			specify "with admin access_token" do
				get url, { access_token: admin.access_token }
			end

			after :each do
				expect(status).to be 200
				expect(json).to have_key :items
				expect(json[:items]).to eq valid_seizure_types_array
			end
		end
	end

	describe "get seizure_reasons" do
		let(:url) { "/api/v1/dictionary/seizure/reasons" }

		context "failure" do
			specify "without access_token" do
				get url
			end

			specify "with invalid access_token" do
				get url, { access_token: "invalid_access_token" }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key :error
			end
		end

		context "success" do
			specify "with patient access_token" do
				get url, { access_token: patient.access_token }
				# binding.pry
			end

			specify "with admin access_token" do
				get url, { access_token: admin.access_token }
			end

			after :each do
				expect(status).to be 200
				expect(json).to have_key :items
				expect(json[:items]).to eq valid_seizure_reasons_array
			end
		end
	end
end