describe "patient_medicine" do

	before :each do
		Array.new(10) { |i| create(:medicine, title: "Medicine #{i}", position: i)  }
	end

	let(:patient) { create :patient }
	let(:another_patient) { create :patient }

	describe "#index" do
		let(:url) { "api/v1/patients/#{patient.id}/medicines" }
		context "failure" do
			specify "without access_token" do
				get url
			end

			specify "with invalid access_token" do
				get url, { access_token: "invalid_access_token" }
			end

			specify "with another patient access_token" do
				get url, { access_token: another_patient.access_token }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key :error
			end
		end

		context "success" do
			it "should return patient's medicines list" do
				get url, { access_token: patient.access_token }

				expect(status).to be 200
				expect(json).to have_key :patient_medicines
				expect(json["patient_medicines"].length).to be 3
				expect(json["patient_medicines"].first).to include :id, :title, :reception
			end
		end
	end

	describe "#create" do
	  let(:url) { "api/v1/patients/#{patient.id}/medicines" }
	  context "unauthorized" do
			specify "without access_token" do
				post url
			end

			specify "with invalid access_token" do
				post url, { access_token: "invalid_access_token" }
			end

			specify "with another patient access_token" do
				post url, { access_token: another_patient.access_token }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key :error
			end
		end

		context "failure" do
			specify "without params" do
				post url, { access_token: patient.access_token }
			end

			after :each do
				expect(status).to be 422
				expect(json).to have_key :errors
				expect(json["errors"]).to include :medicine_id, :beginning_at, :reception
			end
		end

		context "success" do
			it "should create new medicine to patient" do
				post url, { access_token: patient.access_token, patient_medicine: valid_patient_medicine_params }

				expect(status).to be 200
				expect(json).to have_key :patient_medicine
				expect(json["patient_medicine"]).to include :id, :title, :reception
			end
		end
	end

	describe "#destroy" do
		let(:url) { "api/v1/patients/#{patient.id}/medicines/#{patient.medicines.first.id}" }

		context "not found" do
			specify "with invalid ID" do
				delete "api/v1/patients/#{patient.id}/medicines/999", { access_token: patient.access_token }
				expect(status).to be 404
				expect(json).to have_key :error
			end
		end

	  context "unauthorized" do
			specify "without access_token" do
				delete url
			end

			specify "with invalid access_token" do
				delete url, { access_token: "invalid_access_token" }
			end

			specify "with another patient access_token" do
				delete url, { access_token: another_patient.access_token }
			end

			after :each do
				expect(status).to be 401
				expect(json).to have_key :error
			end
	  end

	  context "success" do
	  	it "should delete patient_medicine" do
	  		delete url, { access_token: patient.access_token }
	  	end

	  	after { expect(status).to be 200 }
	  end
	end

end