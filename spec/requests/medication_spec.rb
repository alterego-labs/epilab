describe "Medication" do

	let(:admin) { create(:admin) }
	let(:patient) { create(:patient) }
	let(:another_patient) { create(:patient) }

	before :each do
		Array.new(10) { |i| create(:medicine, title: "Medicine #{i}", position: i)  }
		Patients::ScheduleMedicines.new(patient_id: patient.id, date: Time.zone.now).execute
	end

	describe "#today" do
		let(:url) { "api/v1/patients/#{patient.id}/medications/today" }
		context "failure" do
			specify "without access_token" do
				get url
				expect(status).to be 401
			end

			specify "with invalid access_token" do
				get url, { access_token: "invalid_token" }
				expect(status).to be 401
			end

			specify "pacient not found" do
				get "api/v1/patients/999/medications/today"
				expect(status).to be 404
			end

			specify "with another patient access_token" do
				get url, { access_token: another_patient.access_token }
				expect(status).to be 401
			end

			after { expect(json).to have_key "error" }
		end

		context "success" do
			specify "patient with correct access_token" do
				get url, { access_token: patient.access_token }
			end

			specify "admin with correct access_token" do
				get url, { access_token: admin.access_token }
			end

			after :each do
				expect(status).to be 200
				expect(json).to have_key :medications
				expect(json[:medications]).to eq medications_json_for(patient).map(&:with_indifferent_access)
			end
		end
	end

	describe "#graphic" do
		before :each do
			Medication.find_each {|medication| medication.taken! }
		end

		context "failure" do
			specify "without access_token" do
				get "api/v1/patients/#{patient.id}/medications/graphic"
				expect(status).to be 401
			end

			specify "with invalid access_token" do
				get "api/v1/patients/#{patient.id}/medications/graphic", { access_token: "invalid_token" }
				expect(status).to be 401
			end

			specify "pacient not found" do
				get "api/v1/patients/999/medications/graphic"
				expect(status).to be 404
			end

			specify "with another patient access_token" do
				get "api/v1/patients/#{patient.id}/medications/graphic", { access_token: another_patient.access_token }
				expect(status).to be 401
			end

			after { expect(json).to have_key "error" }
		end

		context "success" do
			specify "patient with correct access_token" do
				get "api/v1/patients/#{patient.id}/medications/graphic", { access_token: patient.access_token }
			end

			specify "admin with correct access_token" do
				get "api/v1/patients/#{patient.id}/medications/graphic", { access_token: admin.access_token }
			end

			after :each do
				expect(status).to be 200
				expect(json).to include :items, :series

				expect(json[:series]).to match_array medications_medicine_series_json.map(&:with_indifferent_access)
				# expect(json[:items]).to match_array medication_items_json.map(&:with_indifferent_access)
			end
		end
	end

	describe "#taken" do
		let(:url) { "api/v1/patients/#{patient.id}/medications/1/taken" }
		context "failure" do
			specify "without access_token" do
				post url
				expect(status).to be 401
			end

			specify "with invalid access_token" do
				post url, { access_token: "invalid_token" }
				expect(status).to be 401
			end

			specify "medication not found" do
				post "api/v1/patients/#{patient.id}/medications/999/taken", { access_token: patient.access_token }
				expect(status).to be 404
			end

			specify "with another patient access_token" do
				post url, { access_token: another_patient.access_token }
				expect(status).to be 401
			end

			after { expect(json).to have_key "error" }
		end

		context "success" do
			specify "patient with correct access_token" do
				post url, { access_token: patient.access_token }
			end

			specify "admin with correct access_token" do
				post url, { access_token: admin.access_token }
			end

			after :each do 
				expect(status).to be 200
				expect(Medication.find(1).status).to eq "taken"
			end
		end
	end

	describe "#rejected" do
		let(:url) { "api/v1/patients/#{patient.id}/medications/1/rejected" }
		context "failure" do
			specify "without access_token" do
				post url
				expect(status).to be 401
			end

			specify "with invalid access_token" do
				post url, { access_token: "invalid_token" }
				expect(status).to be 401
			end

			specify "medication not found" do
				post "api/v1/patients/#{patient.id}/medications/999/rejected", { access_token: patient.access_token }
				expect(status).to be 404
			end

			specify "with another patient access_token" do
				post url, { access_token: another_patient.access_token }
				expect(status).to be 401
			end

			after { expect(json).to have_key "error" }
		end

		context "success" do
			specify "patient with correct access_token" do
				post url, { access_token: patient.access_token }
			end

			specify "admin with correct access_token" do
				post url, { access_token: admin.access_token }
			end

			after :each do 
				expect(status).to be 200
				expect(Medication.find(1).status).to eq "rejected"
			end
		end
	end

end