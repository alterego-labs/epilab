ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)

# require 'minitest/autorun'
require 'rspec/rails'
require 'capybara/rspec'
# require 'rspec/autorun'

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }
ActiveRecord::Migration.check_pending! if defined?(ActiveRecord::Migration)

RSpec.configure do |config|
  config.mock_with :rspec
  config.include Capybara::DSL

  config.include FactoryGirl::Syntax::Methods
  # config.include CaypabaraMacros
  config.include Requests::JsonHelpers
  config.include API::UserHelpers
  config.include API::MedicineHelpers
  config.include API::SeizureHelpers
  config.include API::MedicationHelpers
  config.include API::DictionaryHelpers
  config.include API::PatientMedicineHelpers
  config.include RSpec::Rails::RequestExampleGroup, type: :requests
  config.include Devise::TestHelpers, type: :controller

  # config.raise_errors_for_deprecations!

  config.infer_spec_type_from_file_location!
  config.infer_base_class_for_anonymous_controllers = false
  config.filter_run focus: true
  config.run_all_when_everything_filtered = true

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

end
