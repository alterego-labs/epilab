FactoryGirl.define do
	sequence(:email) { Faker::Internet.email }
	sequence(:first_name) { Faker::Name.first_name }
	sequence(:last_name) { Faker::Name.last_name }
	sequence(:text) { Faker::Lorem.sentence(50) }

	factory :user do
		first_name
		last_name
		email
		password "password"
		sex "male"
		birthday Date.today-20.years

		factory :patient do
			role "patient"
			is_watcher false
			seizures { Array.new(10) { create(:seizure) } }
			medicines { Array.new(3) {|i| create(:patient_medicine, medicine_id: i+1) } }
		end

		factory :admin do
			role "admin"
		end
	end

	factory :medicine do
		sequence(:title) {|n| "Medicine #{n}"}
	end

	factory :seizure do
		sequence(:type) {|n| "Seizure #{n}" }
		sequence(:reason) {|n| "Reason #{n}" }
		duration 10
		sequence(:happened_at) {|n| DateTime.now }
		comment { Faker::Lorem.sentence(50) }
	end

	factory :patient_medicine do
		# time "08:00"
		# dosage 10
		beginning_at Date.yesterday
		periodicity 1
		reception [{time: "8:00", dosage: 100}]
	end

	factory :dictionary_item do
		sequence(:title) {|n| "Dictionary Item #{n}" }
		factory :seizure_type do
			type "seizure_type"
		end
		factory :seizure_reason do
			type "seizure_reason"
		end
	end

	factory :doctor do
		first_name
		last_name
		about "Lorem ipsum dolor sit amet.."
	end

	factory :clinic do
		about "Lorem ipsum dolor sit amet.."
		coordinates "48.7166700 37.5333300"
		address "Москва, Россия"
		phone "(012) 345-67-89"
		email "epilab@medicine.com"
		site "http://epilab.pp.ua"
	end

	factory :consultation do
		text
	end

	factory :dialog do
	end

end