module Requests
  module JsonHelpers
    def json
      @json ||= JSON.parse(response.body).with_indifferent_access
    end

    def status
    	@status ||= response.status
    end
  end
end