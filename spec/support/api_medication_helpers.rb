module API
	module MedicationHelpers
		def medications_json_for(patient)
			ActiveModel::ArraySerializer.new(medications_for(patient), each_serializer: MedicationSerializer).as_json
		end

		def medications_for(patient)
			MedicationQuery.today_for patient.id
		end

		def medications_medicine_series_json
			[
				{ valueField: "medicine-0", name: "Medicine 0" },
				{ valueField: "medicine-1", name: "Medicine 1" },
				{ valueField: "medicine-2", name: "Medicine 2" }
			]
		end

		def medication_items_json
			[
				{
					data: Russian::strftime(Time.zone.now, "%d %b %Y"),
					medicines: {
						"medicine-0" => 10,
						"medicine-1" => 10,
						"medicine-2" => 10	
					}
				}
			]
		end

		def medication_graphic_data_json
			{
				items: medication_items_json,
				series: medications_medicine_series_json
			}
		end

	end
end