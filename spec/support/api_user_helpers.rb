module API
	module UserHelpers
		def correct_access_token
			User.last.access_token
		end

		def latest_user
			UserSerializer.new(User.last, root: false).as_json.stringify_keys
		end

		def correct_auth_data
			{
				"access_token" => correct_access_token,
				"user" => latest_user
			}
		end

		def valid_user_update_params
			{
				"is_watcher" => true,
				"sex" => :female
			}
		end

		def invalid_user_update_params
			{
				"email" => nil
			}
		end

		def invalid_user_password_confirmation_params
			{
				password: "new_password",
				password_confirmation: "incorrect_password",
				current_password: "password"
			}			
		end

		def invalid_user_current_password_params
			{
				password: "new_password",
				password_confirmation: "new_password",
				current_password: "incorrect_password"
			}
		end

		def valid_user_update_password_params
			{
				password: "new_password",
				password_confirmation: "new_password",
				current_password: "password"
			}
		end
	end
end