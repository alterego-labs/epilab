module API
	module DictionaryHelpers
		def valid_seizure_types_array
			["Seizure Type 1", "Seizure Type 0"]
		end

		def valid_seizure_reasons_array
			["Seizure Reason 0", "Seizure Reason 1", "Seizure Reason 2"]
		end
	end
end