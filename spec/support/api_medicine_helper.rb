module API
	module MedicineHelpers
		def medicines_json
			ActiveModel::ArraySerializer.new(Medicine.all, each_serializer: MedicineSerializer).as_json
		end
	end
end