module API
	module PatientMedicineHelpers
		def valid_patient_medicine_params
			{
				medicine_id: 5,
				beginning_at: Date.today,
				periodicity: "everyday",
				remind: true,
				reception: [{
						time: "12:00",
						dosage: 100
					}]
			}
		end
	end
end