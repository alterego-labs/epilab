module API
	module SeizureHelpers
		def seizures_json_for(patient)
			ActiveModel::ArraySerializer.new(seizures_for(patient), each_serializer: SeizureSerializer).as_json
		end

		def seizures_for(patient)
			@_seizures ||= SeizureQuery.today_for(patient.id)
		end

		def seizure_json_for(seizure)
			SeizureSerializer.new(seizure, root: false).as_json
		end

		def valid_seizure_params
			{
				type: "Type",
				duration: 20,
				reason: "Stress",
				happened_at: "01.01.2014 10:00"
			}
		end

		def invalid_seizure_duration_params
			valid_seizure_params.merge({ duration: "string duration is invalid" })
		end
	end
end